unit PreferencesUnit;

interface

uses Vcl.WinXCtrls;

const
  APP_NAME = '�������������� ��������� ����������';
  PAGE_NAMES: TArray<String> = ['First', 'Second', 'Third'];
  // ���� True, ��� ��������� ���������� ����� ���������� ����������� � ���� xml
  ALWAYS_SAVE = True;

  // ��������� ��������� ������ TSplitView
  SV_CLOSE_ON_MENU_CLICK = False;
  SV_USE_ANIMATION = True;
  SV_ANIMATION_DELAY = 75;
  SV_ANIMATION_STEP = 100;
  SV_DISPLAY_MODE = svmDocked;
  SV_PLACEMENT = svpLeft;
  SV_CLOSE_STYLE = svcCompact;

  // ��������� ��������
  ACT_HOME_CAPTION = '������';
  ACT_HOME_INDEX = 0;
  ACT_WIND_CAPTION = '�������� ��������';
  ACT_WIND_INDEX = 1;
  ACT_DESIGN_CAPTION = '���������������';
  ACT_DESIGN_INDEX = 2;
  ACT_MODEL_CAPTION = '�������������';
  ACT_MODEL_INDEX = 3;
  ACT_ANALYSE_CAPTION = '�����������������';
  ACT_ANALYSE_INDEX = 4;
  ACT_HELP_CAPTION = '������';
  ACT_HELP_INDEX = 5;
  ACT_OPT_CAPTION = '�����������';
  ACT_OPT_INDEX = 6;

  ACT_HOME_HINT = '��������� � ������';
  ACT_WIND_HINT = '�������� ������ �� �������� ��������';
  ACT_DESIGN_HINT = '������� � ��������������� ����������';
  ACT_OPT_HINT = '������� � ���� �����������';
  ACT_MODEL_HINT = '������� � ���������� �������������';
  ACT_ANALYSE_HINT = '��������� ����������������� �������� ����� ����������';
  ACT_HELP_HINT = '������� �������';

  ACT_HOME_LONGHINT = '��������� � ������ | ��������� � ������ � ������ ����� ������';
  ACT_WIND_LONGHINT =
    '�������� ������ �� �������� ��������|������ ������������ ���������� �������� ������������ ��� ������� �������� ��������';
  ACT_DESIGN_LONGHINT =
    '������� � ��������������� ����������|����������� �������������� � ������� ���������� ����������';
  ACT_OPT_LONGHINT =
    '������� � ���� �����������|����������� ���������� ���������� �� �������� ��������� ��������';
  ACT_MODEL_LONGHINT =
    '������� � ���������� �������������|��������� ��������������� ������������� �������� ����� ���������� � CAD-�����';
  ACT_ANALYSE_LONGHINT =
    '��������� ����������������� �������� ����� ����������|����������� ����������� �������-���������� ������ �������� ����� ���������� � CAE-�����';
  ACT_HELP_LONGHINT = '������� �������|���������� ������� � ��������� �������� ���������';

  DATA_CAN_CHANGE = 'Variable';
  DATA_INPUT = 'Input';
  DATA_OUTPUT = 'Output';

  ICON_INPUT = 0;
  ICON_LOCKED = 1;
  ICON_UNLOCKED = 2;

  // ������������ �� �������� �� ��������� ��� ����������� ���������� ��� ��������� �� �������
  // 1 - ��������� ����������
  // 0 - ��������� �������
  USE_DEFAULT_VALUES = 1;

const
  STATE_COLUMN_INDEX = 0;
  NAME_COLUMN_INDEX = 1;
  DESC_COLUMN_INDEX = 2;
  VALUE_COLUMN_INDEX = 3;
  HINT_COLUMN_INDEX = 4;

type
  PParamRecord = ^TParamRecord;

  TParamRecord = record
    ParamState: Integer;
    ParamName: string;
    ParamDescription: string;
    ParamValue: string;
    ParamDim: string;
    ParamHint: string;
  end;

const
  szParamRecord = sizeof(TParamRecord);

resourcestring
  StrValueIsNILL = '�������� ��������� "%s" �� ����� ���� ������';
  DEFAULT_PARAMETER_TOLERANCE = '3';

implementation

end.
