unit GeneratorInfoObjectUnit;

interface

uses System.Classes, Xml.XMLIntf, Xml.xmldom;

function StrToFloatSafe(const aStr: String): Extended;

type
  TGeneratorInfo = class(TPersistent)
  private
    FXML: IXMLDocument;
    FGeneratorFullReport: TStringList;
    function GetParamValueByName(ParamName, PropertyName: String): String;
    function SetParamValueByName(ParamName, PropertyName: String; Value: String): HRESULT;
    function CreateAttribute(Name, DefaultValue, Dim, MinValue, MaxValue, Desc, Hint,
      Tolerance: String): IXMLNode;
    function AddParameterCategory(ParamName, CategoryName, CategoryType: String): IXMLNode;
    function NewXMLFileCreation: HRESULT;
    function GetParamAttributeByName(ParamName, AttributeName: String): String;
    function GetParamValueByIndex(ParamIndex: Integer; PropertyName: String): String;
    function GetGeneratorFullReport: TStringList;
    function SaveIfNeeded: HRESULT;
  public
    constructor Create;
    destructor Destroy;
    procedure DesignExecute;
    procedure WindExecute;
    procedure SaveFullReport(FileName: String);
    property FullReport: TStringList read GetGeneratorFullReport;
    function GetParamValue(ParamName: String): String;
    function GetParamCalculatedValue(ParamName: String): String;
    function GetParamDefaultValue(ParamName: String): String;
    function GetParamDesc(ParamName: String): String;
    function GetParamDim(ParamName: String): String;
    function GetParamHint(ParamName: String): String;
    function GetParamMax(ParamName: String): String;
    function GetParamMin(ParamName: String): String;
    function GetParamTol(ParamName: String): String;
    function GetParamState(ParamName: String): Integer;
    function GetParamFullDesc(ParamName: String): String;
    function GetParamFullHint(ParamName: String): String;
    function GetParamFullReport(ParamIndex: Integer): String;
    function GetCategoryParams(CategoryName: String): TStringList;
    function SetParamValue(ParamName: String; Value: Extended): HRESULT; overload;
    function SetParamValue(ParamName: String; Value: Integer): HRESULT; overload;
    function SetParamValue(ParamName: String; Value: String): HRESULT; overload;
    function SetParamCalculatedValue(ParamName: String; Value: Extended): HRESULT; overload;
    function SetParamCalculatedValue(ParamName: String; Value: Integer): HRESULT; overload;
    function SetParamCalculatedValue(ParamName: String; Value: String): HRESULT; overload;
    property Xml: IXMLDocument read FXML write FXML;
    function SaveWordReport(FileName: String): HRESULT;
  end;

var
  GeneratorInfo: TGeneratorInfo;

implementation

uses DesignCalcUnit, System.Math, System.SysUtils, Xml.XMLDoc, PreferencesUnit, Vcl.Dialogs,
  Word_TLB, Vcl.OleCtrls;

// ����������� ������ � ������������ �����. ��� ����, ������������ ����� � �������
// ������ ����� ����� ���� ����� ���� �� ��������� D
function StrToFloatSafe(const aStr: String): Extended;
const
  // ��������� ��������� ������������ ����� � ������� ����� � ������ �����
  D = ['.', ','];
var
  S: String;
  i: Integer;
begin
  S := aStr;
  for i := 1 to Length(S) do
  begin
    if S[i] in D then
    begin
      S[i] := FormatSettings.DecimalSeparator;
      Break;
    end;
  end;
  { try }
  Result := StrToFloat(S);
  { except
    on E: EConvertError do
    raise EConvertError.CreateFmt('�������� ���� �������� %s �� �������� ������', [S]);
    on E: Exception do
    raise Exception.CreateFmt('�������� ����������� ������ � ����� %s', [E.ClassName]);
    end; }
end;

{ TGeneratorInfo }

function TGeneratorInfo.AddParameterCategory(ParamName, CategoryName, CategoryType: String)
  : IXMLNode;
var
  node, childnode: IXMLNode;
  i: Integer;
begin
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
    if (Xml.DocumentElement.ChildNodes[i].NodeName = 'Category') and
      (Xml.DocumentElement.ChildNodes[i].Attributes['Name'] = CategoryName) then
    begin
      node := Xml.DocumentElement.ChildNodes[i];
      Break;
    end;

  if node = nil then
    node := Xml.DocumentElement.AddChild('Category');

  node.Attributes['Name'] := CategoryName;
  node.Attributes['type'] := CategoryType;
  childnode := node.AddChild('Parameter');
  childnode.Text := ParamName;
  Result := node;
end;

constructor TGeneratorInfo.Create;
begin
  inherited;
  FGeneratorFullReport := TStringList.Create;

  // FXML := TXMLDocument.Create('1.xml');
  NewXMLFileCreation;
end;

function TGeneratorInfo.CreateAttribute(Name, DefaultValue, Dim, MinValue, MaxValue, Desc, Hint,
  Tolerance: String): IXMLNode;
var
  node, childnode: IXMLNode;
begin
  node := Xml.DocumentElement.AddChild('Parameter');
  node.Attributes['Name'] := Name;
  childnode := node.AddChild('Value');
  childnode.Text := '';
  childnode := node.AddChild('DefaultValue');
  childnode.Text := DefaultValue;
  childnode := node.AddChild('CalculatedValue');
  childnode.Text := '';
  childnode := node.AddChild('Dimension');
  childnode.Text := Dim;
  childnode := node.AddChild('MinValue');
  childnode.Text := MinValue;
  childnode := node.AddChild('MaxValue');
  childnode.Text := MaxValue;
  childnode := node.AddChild('Description');
  childnode.Text := Desc;
  childnode := node.AddChild('Hint');
  childnode.Text := Hint;
  childnode := node.AddChild('Tolerance');
  childnode.Text := Tolerance;
end;

procedure TGeneratorInfo.DesignExecute;
var
  DesignOutputData: TDesignOutputData;
  DesignInputData: TDesignInputData;
begin

  if (USE_DEFAULT_VALUES = 1) then
  begin
    if GetParamValue('PMM') = '' then
      SetParamValue('PMM', GetParamDefaultValue('PMM'));

    if GetParamValue('U') = '' then
      SetParamValue('U', GetParamDefaultValue('U'));

    if GetParamValue('F1') = '' then
      SetParamValue('F1', GetParamDefaultValue('F1'));

    if GetParamValue('AL') = '' then
      SetParamValue('AL', GetParamDefaultValue('AL'));

    if GetParamValue('B0') = '' then
      SetParamValue('B0', GetParamDefaultValue('B0'));

    if GetParamValue('BL') = '' then
      SetParamValue('BL', GetParamDefaultValue('BL'));

    if GetParamValue('D11') = '' then
      SetParamValue('D11', GetParamDefaultValue('D11'));

    if GetParamValue('DI') = '' then
      SetParamValue('DI', GetParamDefaultValue('DI'));

    if GetParamValue('P') = '' then
      SetParamValue('P', GetParamDefaultValue('P'));
  end;

  DesignInputData.PMM := StrToFloatSafe(GetParamValue('PMM'));
  DesignInputData.U := StrToFloatSafe(GetParamValue('U'));
  DesignInputData.F1 := StrToFloatSafe(GetParamValue('F1'));
  DesignInputData.AL := StrToFloatSafe(GetParamValue('AL')) / 1000;
  DesignInputData.B0 := StrToFloatSafe(GetParamValue('B0')) / 1000;
  DesignInputData.BL := StrToFloatSafe(GetParamValue('BL'));
  DesignInputData.D11 := StrToFloatSafe(GetParamValue('D11'));
  DesignInputData.DI := StrToFloatSafe(GetParamValue('DI')) / 1000;
  DesignInputData.P := StrToInt(GetParamValue('P'));

  MainCalc(DesignInputData, DesignOutputData);

  // SetParamCalculatedValue('AL2', DesignOutputData.AL2);
  SetParamCalculatedValue('ALW', 1000 * DesignOutputData.ALW);
  SetParamCalculatedValue('B5', 1000 * DesignOutputData.B5);
  SetParamCalculatedValue('DI1', 1000 * DesignOutputData.DI);
  SetParamCalculatedValue('BBP', 1000 * DesignOutputData.bbp);
  SetParamCalculatedValue('BL1', DesignOutputData.BL);
  SetParamCalculatedValue('BMM', 1000 * DesignOutputData.bmm);
  SetParamCalculatedValue('BZ1', DesignOutputData.bz1);
  SetParamCalculatedValue('CPR', DesignOutputData.cpr);
  SetParamCalculatedValue('CS', DesignOutputData.cs);
  SetParamCalculatedValue('CS1', DesignOutputData.CS1);
  SetParamCalculatedValue('DCP', 1000 * DesignOutputData.Dcp);
  SetParamCalculatedValue('DE', 1000 * DesignOutputData.De);
  SetParamCalculatedValue('DN', 1000 * DesignOutputData.DN);
  SetParamCalculatedValue('DNP', 1000 * DesignOutputData.Dnp);
  SetParamCalculatedValue('E0', DesignOutputData.e0);
  SetParamCalculatedValue('EDK', DesignOutputData.edk);
  SetParamCalculatedValue('F11', DesignOutputData.f11);
  SetParamCalculatedValue('FAD', DesignOutputData.FAD);
  SetParamCalculatedValue('FBO', DesignOutputData.Fbo);
  SetParamCalculatedValue('FDK', 1000 * DesignOutputData.fdk);
  SetParamCalculatedValue('FFM', DesignOutputData.FFM);
  SetParamCalculatedValue('FJ', DesignOutputData.Fj);
  SetParamCalculatedValue('FL', DesignOutputData.FL);
  SetParamCalculatedValue('FMK', 1000 * DesignOutputData.fmk);
  SetParamCalculatedValue('FMK1', 1000 * DesignOutputData.fmk1);
  SetParamCalculatedValue('FN', 1000 * DesignOutputData.Fn);
  SetParamCalculatedValue('FQ', 1000 * DesignOutputData.fq);
  SetParamCalculatedValue('FSM', 1000 * DesignOutputData.fsm);
  SetParamCalculatedValue('FZ1', DesignOutputData.Fz1);
  SetParamCalculatedValue('H0', DesignOutputData.H0);
  SetParamCalculatedValue('H1', 1000 * DesignOutputData.h1);
  SetParamCalculatedValue('H2', DesignOutputData.H2);
  SetParamCalculatedValue('H3', DesignOutputData.H3);
  SetParamCalculatedValue('H45', DesignOutputData.H45);
  SetParamCalculatedValue('H5', DesignOutputData.H5);
  SetParamCalculatedValue('H8', DesignOutputData.H8);
  SetParamCalculatedValue('H9', DesignOutputData.H9);
  SetParamCalculatedValue('HA', 1000 * DesignOutputData.ha);
  SetParamCalculatedValue('HGEN', 1000 * DesignOutputData.HGEN);
  SetParamCalculatedValue('HM', 1000 * DesignOutputData.hm);
  SetParamCalculatedValue('HU1', 100 * DesignOutputData.HU1 { ��� } );
  SetParamCalculatedValue('LM', 1000 * DesignOutputData.lm);
  SetParamCalculatedValue('LSL', 1000 * DesignOutputData.lsl);
  SetParamCalculatedValue('PCZ', DesignOutputData.PCZ);
  SetParamCalculatedValue('PE', DesignOutputData.Pe);
  SetParamCalculatedValue('PL', DesignOutputData.PL);
  SetParamCalculatedValue('PL2', DesignOutputData.PL2);
  SetParamCalculatedValue('PMAX', DesignOutputData.PMax / 1000);
  SetParamCalculatedValue('PX1', DesignOutputData.PX1);
  SetParamCalculatedValue('RM', DesignOutputData.RM);
  SetParamCalculatedValue('SUMP', DesignOutputData.SUMP);
  SetParamCalculatedValue('T1', 1000 * DesignOutputData.t1);
  SetParamCalculatedValue('TAY', 1000 * DesignOutputData.tay);
  SetParamCalculatedValue('TOKA', DesignOutputData.TOKA);
  SetParamCalculatedValue('TOKR', DesignOutputData.TOKR);
  SetParamCalculatedValue('U11', DesignOutputData.U11);
  SetParamCalculatedValue('VM', 1E6 * DesignOutputData.vm { ����� �������� } );
  SetParamCalculatedValue('WR', DesignOutputData.wr);
  SetParamCalculatedValue('XAD', DesignOutputData.XAD);
  SetParamCalculatedValue('XAQ', DesignOutputData.xaq);
  SetParamCalculatedValue('XD', DesignOutputData.xd);
  SetParamCalculatedValue('XQ', DesignOutputData.xq);
  SetParamCalculatedValue('XS', DesignOutputData.xs);
  SetParamCalculatedValue('XSP', DesignOutputData.xsp);

end;

destructor TGeneratorInfo.Destroy;
begin
  FreeAndNil(FGeneratorFullReport);
  inherited;
end;

function TGeneratorInfo.GetParamValue(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'Value');
end;

function TGeneratorInfo.GetParamValueByIndex(ParamIndex: Integer; PropertyName: String): String;
var
  a, b, c, D: String;
begin
  if Xml.DocumentElement.ChildNodes[ParamIndex].NodeName = 'Parameter' then
    Result := Xml.DocumentElement.ChildNodes[ParamIndex].ChildNodes[PropertyName].Text
  else
    Result := '';
  { raise Exception.Create(Format('�� ������� �������� %s � ��������� %s � ������� %d',
    [PropertyName, Xml.DocumentElement.ChildNodes[ParamIndex].Attributes['Name'], ParamIndex])); }
end;

function TGeneratorInfo.GetParamValueByName(ParamName, PropertyName: String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
    if (Xml.DocumentElement.ChildNodes[i].NodeName = 'Parameter') and
      (Xml.DocumentElement.ChildNodes[i].Attributes['Name'] = ParamName) then
      try
        Result := Xml.DocumentElement.ChildNodes[i].ChildNodes[PropertyName].Text;
      except
        raise Exception.Create(Format('�� ������� �������� %s � ��������� %s',
          [PropertyName, ParamName]));
      end;
end;

function TGeneratorInfo.GetParamAttributeByName(ParamName, AttributeName: String): String;
var
  i: Integer;
begin
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
    if (Xml.DocumentElement.ChildNodes[i].NodeName = 'Parameter') and
      (Xml.DocumentElement.ChildNodes[i].Attributes['Name'] = ParamName) then
      Result := Xml.DocumentElement.ChildNodes[i].Attributes[AttributeName];
end;

function TGeneratorInfo.GetParamCalculatedValue(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'CalculatedValue');
end;

function TGeneratorInfo.GetParamTol(ParamName: String): String;
begin
  if GetParamValueByName(ParamName, 'Tolerance') <> '' then
    Result := GetParamValueByName(ParamName, 'Tolerance')
  else
    Result := DEFAULT_PARAMETER_TOLERANCE;
end;

function TGeneratorInfo.GetParamDefaultValue(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'DefaultValue');
end;

function TGeneratorInfo.GetParamDim(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'Dimension');
end;

function TGeneratorInfo.GetParamFullDesc(ParamName: String): String;
begin
  if GetParamDim(ParamName) <> '' then
    Result := GetParamDesc(ParamName) + ', ' + GetParamDim(ParamName)
  else
    Result := GetParamDesc(ParamName);
end;

function TGeneratorInfo.GetParamFullHint(ParamName: String): String;
begin
  if (GetParamMin(ParamName) <> '') and (GetParamMax(ParamName) <> '') then
    Result := GetParamHint(ParamName) + ' � ��������� (' + GetParamMin(ParamName) + '-' +
      GetParamMax(ParamName) + ')'
  else
    Result := GetParamHint(ParamName);
end;

function TGeneratorInfo.GetParamFullReport(ParamIndex: Integer): String;
var
  CurrValue: String;
begin
  if Xml.DocumentElement.ChildNodes[ParamIndex].NodeName = 'Parameter' then
  begin
    if (GetParamValueByIndex(ParamIndex, 'CalculatedValue') <> '') then
      CurrValue := GetParamValueByIndex(ParamIndex, 'CalculatedValue');

    if (GetParamValueByIndex(ParamIndex, 'Value') <> '') then
      CurrValue := GetParamValueByIndex(ParamIndex, 'Value');

    if (GetParamValueByIndex(ParamIndex, 'CalculatedValue') = '') and
      (GetParamValueByIndex(ParamIndex, 'Value') = '') then
      Result := ''
    else
      Result := GetParamValueByIndex(ParamIndex, 'Description') + ': ' + CurrValue + ' ' +
        GetParamValueByIndex(ParamIndex, 'Dimension')
  end
  else
    Result := '';
end;

function TGeneratorInfo.GetCategoryParams(CategoryName: String): TStringList;
var
  i, n: Integer;
  SL: TStringList;
begin
  SL := TStringList.Create;
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
  begin
    if (Xml.DocumentElement.ChildNodes[i].NodeName = 'Category') and
      (Xml.DocumentElement.ChildNodes[i].Attributes['Name'] = CategoryName) then
    begin
      for n := 0 to Xml.DocumentElement.ChildNodes[i].ChildNodes.Count - 1 do
      begin
        if (Xml.DocumentElement.ChildNodes[i].ChildNodes[n].NodeName = 'Parameter') then
          SL.Add(Xml.DocumentElement.ChildNodes[i].ChildNodes[n].Text)
      end;
    end;
  end;
  Result := SL;
end;

function TGeneratorInfo.GetGeneratorFullReport(): TStringList;
var
  i: Integer;
begin
  FGeneratorFullReport.Clear;
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
  begin
    if GeneratorInfo.GetParamFullReport(i) <> '' then
      FGeneratorFullReport.Add(GeneratorInfo.GetParamFullReport(i));
  end;
  Result := FGeneratorFullReport;
end;

function TGeneratorInfo.GetParamDesc(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'Description');
end;

function TGeneratorInfo.GetParamMin(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'MinValue');
end;

function TGeneratorInfo.GetParamState(ParamName: String): Integer;
begin
  if (GetParamValue(ParamName) = '') and (GetParamCalculatedValue(ParamName) = '') then
    Result := ICON_INPUT;
  if (GetParamValue(ParamName) = '') and (GetParamCalculatedValue(ParamName) <> '') then
    Result := ICON_LOCKED;
  if (GetParamValue(ParamName) <> '') and (GetParamCalculatedValue(ParamName) <> '') then
    Result := ICON_UNLOCKED;
end;

function TGeneratorInfo.GetParamMax(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'MaxValue');
end;

function TGeneratorInfo.GetParamHint(ParamName: String): String;
begin
  Result := GetParamValueByName(ParamName, 'Hint');
end;

function TGeneratorInfo.NewXMLFileCreation: HRESULT;
begin
  Xml := NewXMLDocument;
  Xml.Options := Xml.Options + [doNodeAutoIndent];
  // Xml.LoadFromFile('GeneratorTemplate.xml');

  Xml.Encoding := 'UTF-8';
  Xml.AddChild('Generator');

  CreateAttribute('VWIND', '6', '�/�', '1', '12', '�������� �����',
    '������� ������� �������� ����� �� ������ 10 � � ����� ���������', '1');
  CreateAttribute('RBL', '1,5', '�', '0,2', '10', '����� �������',
    '������� �������� ����� ������� �������� �������', '1');

  CreateAttribute('AL', '40', '��', '20', '65', '�������� �����',
    '������� �������� ����� ����������', '0');
  CreateAttribute('B0', '1', '��', '0,5', '3', '��������� �����',
    '������� ������� ��������� ��������������� ����� ����� ����������� ��������� ������ � ���������� �������',
    '1');
  CreateAttribute('BL', '0,7', '��', '0,5', '1,2', '�������� � ���� �������',
    '������� ��������� �������� � ���� �������', '2');
  CreateAttribute('D11', '4,5', '�/��.��', '4', '6', '��������� ����',
    '������� ��������� �������������� ���� � ����������', '2');
  CreateAttribute('DI', '300', '��', '200', '400', '�������� �������',
    '������� ���������� �������� ������� ����������', '0');
  CreateAttribute('F1', '50', '��', '40', '100', '������� ����',
    '������� ������� ����������� �������������� ���� ����', '0');
  CreateAttribute('P', '16', '', '4', '64', '����� ��� �������', '������� ����� ��� �������', '0');
  CreateAttribute('PMM', '3000', '��', '100', '5000', '��������',
    '������� ��������� �������� ����������', '0');
  CreateAttribute('U', '220', '�', '100', '400', '���������� ����',
    '������� ���������� ����������� �������������� ���� ����', '0');

  CreateAttribute('RM', '', '��', '', '', '�������� ������������� ������� �������',
    '������� �������� ��������� ������������� ������� �������', '3');
  CreateAttribute('TOKA', '', '�', '', '', '�������� ���', '������� �������� ��������� ����', '2');
  CreateAttribute('DI1', '', '��', '200', '400', '���������� ���������� �������� �������',
    '������� �������� ����������� ����������� ��������� �������� ����������', '0');
  CreateAttribute('HGEN', '', '��', '', '', '������ ����������',
    '������� �������� ������ ����������', '0');
  CreateAttribute('HM', '', '��', '', '', '������ �������',
    '������� �������� ������ ����������� ������� ������', '1');
  CreateAttribute('HA', '', '��', '', '', '������ ���� �������',
    '������� �������� ������ ���� �������', '0');
  CreateAttribute('XAQ', '', '��', '', '',
    '������� ����������� ������������� ������� ������� �� ���������� ���',
    '������� �������� �������� ������������ ������������� ������� ������� �� ���������� ���', '3');
  CreateAttribute('XAD', '', '��', '', '', '������� ����������� ������������� �� ���������� ���',
    '������� �������� �������� ������������ ������������� ������� ������� �� ���������� ���', '3');
  CreateAttribute('DNP', '', '��', '', '', '������� �������',
    '������� �������� �������� ������� ������� �������', '6');
  CreateAttribute('LSL', '', '��', '', '', '����� ������� ����� �������',
    '������� �������� ����� ������� ����� ������� �������', '0');
  CreateAttribute('LM', '', '��', '', '', '����� �������',
    '������� �������� ����� ����������� ������� ������', '1');
  CreateAttribute('T1', '', '��', '', '', '�������� ��� ������� �� ����������� ��������',
    '������� �������� ��������� ���� ������� �� ����������� �������� �������', '0');
  CreateAttribute('XS', '', '��', '', '', '����������� ������������� ������� �������',
    '������� �������� ������������ ������������� ������� �������', '3');
  CreateAttribute('XQ', '', '��', '', '', '����������� ������������� ����� �� ���������� ���',
    '������� �������� ������������ ������������� ������� ����� ������� �� ���������� ���', '3');
  CreateAttribute('XD', '', '��', '', '', '����������� ������������� ����� �� ���������� ���',
    '������� �������� ������������ ������������� ������� ����� ������� �� ���������� ���', '3');
  CreateAttribute('BZ1', '', '��', '', '', '�������� � ������ �������',
    '������� �������� ��������� �������� � ������ ������� �������', '2');
  CreateAttribute('BL1', '', '��', '', '', '�������� � ������� ������',
    '������� �������� ��������� �������� � ������� ��������� ��������������� ������ ����� ����������� ��������� ������ � ���������� �������',
    '4');
  CreateAttribute('HU1', '', '%', '0', '100', '��� ����������',
    '������� �������� ������������ ��������� �������� ����������', '1');
  CreateAttribute('PX1', '', '��', '', '', '��������� ������ � ������������������ �����',
    '������� �������� ��������� ������ � ������������������ �����', '2');
  CreateAttribute('PCZ', '', '��', '', '', '��������� ������ ������ �������',
    '������� �������� ��������� ������ � ������ ������� �������', '2');
  CreateAttribute('FMK1', '', '���', '', '', '��������� ����� ����������',
    '������� �������� ��������� ���������� ������ ����������', '2');
  CreateAttribute('PMAX', '', '��', '', '', '������������ ��������',
    '������� �������� ������������ ���������� �������� ����������', '2');
  CreateAttribute('H8', '', '��', '', '', '����� ����� �������',
    '������� �������� ����� ����� �������, �� ������� �������� ������� �������', '2');
  CreateAttribute('H0', '', '��', '', '', '����� �������� � ������',
    '������� �������� ����� ���������� �������� ������', '2');
  CreateAttribute('H9', '', '��', '', '', '����� ���������� ���� �������',
    '������� �������� ����� ���������� ���� � �������� �������', '2');
  CreateAttribute('H5', '', '��', '', '', '����� ������������������ ����� ������ �������',
    '������� �������� ����� ������������������ ����� ������ ������� �������', '2');
  CreateAttribute('H3', '', '��', '', '', '����� ������������������ ����� ���� ������',
    '������� �������� ����� ������������������ ����� ���� ������', '2');
  CreateAttribute('FZ1', '', '�', '', '', '��� ������ �������',
    '������� �������� ��������������� ���� ������ ������� �������', '2');
  CreateAttribute('F11', '', '�', '', '', '��� �������',
    '������� �������� ��������������� ���� �������', '3');
  CreateAttribute('FJ', '', '�', '', '', '��� ���� �������',
    '������� �������� ��������������� ���� ���� �������', '2');
  CreateAttribute('U11', '', '�', '', '', '����������',
    '������� �������� ������������ ���������� ����������� �������������� ���� ����', '2');
  CreateAttribute('DE', '', '��', '', '', '�������� �������� �������',
    '������� �������� ��������� ��������� �������� ����������', '0');
  CreateAttribute('DN', '', '��', '', '', '�������� ������� ����������',
    '������� �������� ��������� �������� ����������', '0');
  CreateAttribute('VM', '', '���. c�', '', '', '����� �������� ������',
    '������� �������� ���������� ������ ���������� �������� ������', '2');
  CreateAttribute('TAY', '', '��', '', '', '�������� ������� �� �������� ��������',
    '������� �������� ��������� ������� �� �������� ��������', '2');
  CreateAttribute('FMK', '', '���', '', '', '����� �������',
    '������� �������� ���������� ������ ����������� ������� ������', '2');
  CreateAttribute('FSM', '', '���', '', '', '����� ��������� �������',
    '������� �������� ���������� ������ ��������� ����������� ������� ������', '2');
  CreateAttribute('PL', '', '��. ��', '', '', '��������� ������� ������� ������� �������',
    '������� �������� ���������� ������� ������� ������� �������', '3');
  CreateAttribute('XSP', '', '��', '', '', '��������� ������������� ����������� ���� ��',
    '������� �������� ���������� ������������� ����������� ���� ��������� ���������', '3');
  CreateAttribute('TOKR', '', '�', '', '', '���������� ���',
    '������� �������� ����������� ����', '2');
  CreateAttribute('DCP', '', '��', '', '', '������� �������� �������',
    '������� �������� �������� ��������� �������� ����������', '0');
  CreateAttribute('FBO', '', '�', '', '', '�������� ���',
    '������� �������� ��������� ��������������� ���� ����������', '3');
  CreateAttribute('H45', '', '��', '', '', '��������� ����� ����������',
    '������� �������� ��������� ����� ����������', '2');
  CreateAttribute('SUMP', '', '��', '', '', '��������� ������ ����������',
    '������� �������� ��������� ������ ����������', '2');
  CreateAttribute('FN', '', '���', '', '', '��������� ����� ����������',
    '������� �������� ���������� ���������� ������ ����������', '2');
  CreateAttribute('PL2', '', '��. ��', '', '', '���������� �������� ������� �������',
    '������� �������� ���������� ������� ������� ������� ������ �������', '3');
  CreateAttribute('WR', '', '', '', '', '����� ������ ���� �������',
    '������� �������� ����� ������ ����� ���� �������', '3');
  CreateAttribute('CS', '', '', '', '', '����� ������ �������',
    '������� �������� ����� ������ ������� �������', '0');
  CreateAttribute('CPR', '', '', '', '', '����� ����������� � ���� �������',
    '������� �������� ����� ����������� � ���� ������� �������', '0');
  CreateAttribute('BMM', '', '��', '', '', '������ �������',
    '������� �������� ������ ����������� ������� ������', '1');
  CreateAttribute('B5', '', '��', '', '', '������ ���� �������',
    '������� �������� ������ ���� ������� �������', '0');
  CreateAttribute('BBP', '', '��', '', '', '������ ������ �� �������� ��������',
    '������� �������� ������ ������ �� �������� ��������', '2');
  CreateAttribute('EDK', '', '�', '', '', '��� �������������� �� ���������� ���',
    '������� �������� ��������������� ���� �������������� ���������� �� ���������� ���', '2');
  CreateAttribute('E0', '', '�', '', '', '��� ��������� ����',
    '������� �������� ��������������� ���� ��������� ���� ����������', '2');
  CreateAttribute('PE', '', '��', '', '', '������������� ������ ������� �������',
    '������� �������� ������������� ������ ������� �������', '2');
  CreateAttribute('ALW', '', '��', '', '', '������� ����� �����',
    '������� �������� ������� ����� ����� ������� �������', '0');
  CreateAttribute('CS1', '', '', '', '', '���������� ����� ������ �������',
    '������� �������� ����������� ����� ������ ������� �������', '0');
  CreateAttribute('FAD', '', '�', '', '', '��� ����� �� ���������� ��� ����������',
    '������� �������� ��������������� ���� ����� �� ���������� ���', '3');
  CreateAttribute('FDK', '', '���', '', '', '����� �������������� �� ���������� ���',
    '������� �������� ���������� ������ �������������� �� ���������� ���', '2');
  CreateAttribute('FFM', '', '�', '', '', '��� �������',
    '������� �������� ��������������� ���� ����������� ������� ������', '3');
  CreateAttribute('FL', '', '�', '', '', '��� ���������� ������',
    '������� �������� ��������������� ���� �������� ���������� ���������������� ������ ����� ����������� ��������� ������ � ���������� �������',
    '3');
  CreateAttribute('FQ', '', '���', '', '', '��������� �����',
    '������� �������� ���������� ������', '2');
  CreateAttribute('H1', '', '��', '', '', '������ ����� �������',
    '������� �������� ������ ����� ������� �������', '1');
  CreateAttribute('H2', '', '��', '', '', '����� ������������������ ����� ���� �������',
    '������� �������� ����� ������������������ ����� ���� �������', '2');

  AddParameterCategory('VWIND', 'WindInput', DATA_INPUT);
  AddParameterCategory('RBL', 'WindInput', DATA_INPUT);

  AddParameterCategory('AL', 'DesignInput', DATA_INPUT);
  AddParameterCategory('B0', 'DesignInput', DATA_INPUT);
  AddParameterCategory('BL', 'DesignInput', DATA_INPUT);
  AddParameterCategory('D11', 'DesignInput', DATA_INPUT);
  AddParameterCategory('DI', 'DesignInput', DATA_INPUT);
  AddParameterCategory('P', 'DesignInput', DATA_INPUT);
  AddParameterCategory('PMM', 'DesignInput', DATA_INPUT);
  AddParameterCategory('U', 'DesignInput', DATA_INPUT);

  AddParameterCategory('ALW', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('B5', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('BBP', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('BL1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('BMM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('BZ1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('CPR', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('CS', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('CS1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('DCP', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('DE', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('DI1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('DN', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('DNP', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('E0', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('EDK', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('F11', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FAD', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FBO', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FDK', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FFM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FJ', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FL', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FMK', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FMK1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FN', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FQ', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FSM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('FZ1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H0', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H2', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H3', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H45', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H5', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H8', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('H9', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('HA', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('HGEN', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('HM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('HU1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('LM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('LSL', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PCZ', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PE', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PL', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PL2', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PMAX', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('PX1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('RM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('SUMP', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('T1', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('TAY', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('TOKA', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('TOKR', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('U11', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('VM', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('WR', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XAD', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XAQ', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XD', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XQ', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XS', 'DesignOutput', DATA_OUTPUT);
  AddParameterCategory('XSP', 'DesignOutput', DATA_OUTPUT);

  Xml.SaveToFile('GeneratorTemplate.xml');
end;

procedure TGeneratorInfo.SaveFullReport(FileName: String);
begin
  FullReport.SaveToFile(FileName);
end;

function TGeneratorInfo.SaveIfNeeded: HRESULT;
begin
  if ALWAYS_SAVE then
    Xml.SaveToFile('Generator.xml');
end;

function TGeneratorInfo.SaveWordReport(FileName: String): HRESULT;
var
  GeneratorStringList: TStringList;
  WordApp: WordApplication;
  Docs: Documents;
  Doc: WordDocument;
  i: Integer;
begin
  GeneratorStringList := GetGeneratorFullReport;
  WordApp := CoWordApplication.Create;
  WordApp.Visible := True;
  Docs := WordApp.Documents;
  Doc := Docs.Add('Normal', False, EmptyParam, True);

  Doc := (WordApp.Documents.Item(1) as WordDocument);

  for i := 0 to GeneratorStringList.Count - 1 do
    Doc.Paragraphs.Item(i + 1).Range.Text := GeneratorStringList.Strings[i] + #13;

end;

function TGeneratorInfo.SetParamValueByName(ParamName, PropertyName, Value: String): HRESULT;
var
  i: Integer;
begin
  for i := 0 to Xml.DocumentElement.ChildNodes.Count - 1 do
    if (Xml.DocumentElement.ChildNodes[i].NodeName = 'Parameter') and
      (Xml.DocumentElement.ChildNodes[i].Attributes['Name'] = ParamName) then
      Xml.DocumentElement.ChildNodes[i].ChildNodes[PropertyName].Text := Value;

  SaveIfNeeded;
end;

procedure TGeneratorInfo.WindExecute;
var
  r, v, PMM: Extended;
begin
  if (USE_DEFAULT_VALUES = 1) then
  begin
    if GetParamValue('VWIND') = '' then
      SetParamValue('VWIND', GetParamDefaultValue('VWIND'));

    if GetParamValue('RBL') = '' then
      SetParamValue('RBL', GetParamDefaultValue('RBL'));
  end;

  r := StrToFloatSafe(GetParamValue('RBL'));
  v := StrToFloatSafe(GetParamValue('VWIND'));

  PMM := 0.5 * 1.23 * 3.14 * r * r * v * v * v * 16 / 27;

  SetParamCalculatedValue('PMM', PMM);
end;

function TGeneratorInfo.SetParamValue(ParamName: String; Value: Extended): HRESULT;
begin
  Result := E_UNEXPECTED;
  if (GetParamMin(ParamName) <> '') and (Value < StrToFloat(GetParamMin(ParamName))) then
    raise Exception.CreateFmt
      ('�������� ���� �������� ��������� %s ������ %f ������ ���������� ����������� %s ��� ����� ���������',
      [ParamName, Value, GetParamMin(ParamName)]);

  if (GetParamMax(ParamName) <> '') and (Value > StrToFloat(GetParamMax(ParamName))) then
    raise Exception.CreateFmt
      ('�������� ���� �������� ��������� %s ������ %f ������ ����������� ����������� %s ��� ����� ���������',
      [ParamName, Value, GetParamMax(ParamName)]);

  SetParamValueByName(ParamName, 'Value', FloatToStr(Value));
  Result := S_OK;
end;

function TGeneratorInfo.SetParamValue(ParamName: String; Value: Integer): HRESULT;
begin
  SetParamValueByName(ParamName, 'Value', IntToStr(Value));
end;

function TGeneratorInfo.SetParamCalculatedValue(ParamName: String; Value: Extended): HRESULT;
var
  Tol: Integer;
begin
  try
    Tol := StrToInt(GetParamTol(ParamName));
    if Tol <> 0 then
    begin
      Value := round(power(10, Tol) * Value) / power(10, Tol);
      SetParamValueByName(ParamName, 'CalculatedValue', FloatToStr(Value));
    end
    else
    begin
      SetParamValueByName(ParamName, 'CalculatedValue', IntToStr(round(Value)));
    end;
  except
    raise Exception.Create(Format('�� ������� �������� � �������� %s ������������ �������� %f',
      [ParamName, Value]));
  end;
end;

function TGeneratorInfo.SetParamCalculatedValue(ParamName: String; Value: Integer): HRESULT;
begin
  try
    SetParamValueByName(ParamName, 'Value', IntToStr(Value));
    SetParamValueByName(ParamName, 'CalculatedValue', IntToStr(Value));
  except
    raise Exception.Create(Format('�� ������� �������� � �������� %s ������������ �������� %d',
      [ParamName, Value]));
  end;
end;

function TGeneratorInfo.SetParamCalculatedValue(ParamName, Value: String): HRESULT;
begin
  try
    SetParamValueByName(ParamName, 'Value', Value);
    SetParamValueByName(ParamName, 'CalculatedValue', Value);
  except
    raise Exception.Create(Format('�� ������� �������� � �������� %s ������������ �������� %s',
      [ParamName, Value]));
  end;
end;

function TGeneratorInfo.SetParamValue(ParamName, Value: String): HRESULT;
// ���������� ���� "��������" ��� ��������� � ������ ParamName
begin
  { if Value = '' then
    raise Exception.Create(Format(StrValueIsNILL, [ParamName])); }

  SetParamValueByName(ParamName, 'Value', Value);
end;

end.
