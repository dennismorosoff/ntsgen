program NTSGen;

uses
  Vcl.Forms,
  MainFormUnit in 'MainFormUnit.pas' {MainForm},
  Vcl.Themes,
  Vcl.Styles,
  PreferencesUnit in 'PreferencesUnit.pas',
  ComplexUnit in 'ComplexUnit.pas',
  Office_TLB in 'Office_TLB.pas',
  Word_TLB in 'Word_TLB.pas',
  VBIDE_TLB in 'VBIDE_TLB.pas',
  DesignCalcUnit in 'DesignCalcUnit.pas',
  SldWorks_TLB in 'SldWorks_TLB.pas',
  SwConst_TLB in 'SwConst_TLB.pas',
  GeneratorInfoObjectUnit in 'GeneratorInfoObjectUnit.pas';

{$R *.res}

begin
  Vcl.Forms.Application.Initialize;
  Vcl.Forms.Application.MainFormOnTaskbar := True;
  Vcl.Forms.Application.Title := 'Генератор';
  Vcl.Forms.Application.CreateForm(TMainForm, MainForm);
  Vcl.Forms.Application.Run;
end.
