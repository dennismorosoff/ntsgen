unit MainFormUnit;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes,
  System.ImageList,
  System.Actions,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.WinXCtrls,
  Vcl.StdCtrls,
  Vcl.CategoryButtons,
  Vcl.Buttons,
  Vcl.ImgList,
  Vcl.Imaging.PngImage,
  Vcl.ComCtrls,
  Vcl.ActnList, Vcl.StdActns, Vcl.ActnMan,
  Vcl.ToolWin, Vcl.ActnCtrls, Vcl.ActnMenus, Vcl.Menus, CommCtrl, Vcl.PlatformDefaultStyleActnCtrls,
  VirtualTrees, VTHeaderPopup, System.Win.TaskbarCore, Vcl.Taskbar;

type
  TMainForm = class(TForm)
    pnlToolbar: TPanel;
    SV: TSplitView;
    catMenuItems: TCategoryButtons;
    lstLog: TListBox;
    imlIcons: TImageList;
    imgMenu: TImage;
    MainActionList: TActionList;
    actHome: TAction;
    actWind: TAction;
    actDesign: TAction;
    lblTitle: TLabel;
    actOpt: TAction;
    actModel: TAction;
    actAnalyse: TAction;
    actHelp: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    EditSelectAll1: TEditSelectAll;
    EditUndo1: TEditUndo;
    EditDelete1: TEditDelete;
    HelpContents1: THelpContents;
    HelpTopicSearch1: THelpTopicSearch;
    HelpOnHelp1: THelpOnHelp;
    HelpContextAction1: THelpContextAction;
    FileOpen1: TFileOpen;
    FileSaveAs1: TFileSaveAs;
    ActionManager1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    ImageListIcons: TImageList;
    StatusBar1: TStatusBar;
    MainPanel: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ButtonDesign: TButton;
    ComboBox1: TComboBox;
    VirtualStringTree1: TVirtualStringTree;
    ButtonModel: TButton;
    MainPageControl: TPageControl;
    TabSheetModel: TTabSheet;
    TabSheetDesign: TTabSheet;
    TabSheetWind: TTabSheet;
    HeaderPanel: TPanel;
    ButtonWind: TButton;
    TabSheetHome: TTabSheet;
    TabSheetAnalyse: TTabSheet;
    TabSheetHelp: TTabSheet;
    ButtonAnalyse: TButton;
    Label4: TLabel;
    Label5: TLabel;
    actReport: TAction;
    SearchBoxByNameParam: TSearchBox;
    Taskbar1: TTaskbar;
    procedure FormCreate(Sender: TObject);
    procedure SVClosed(Sender: TObject);
    procedure SVClosing(Sender: TObject);
    procedure SVOpened(Sender: TObject);
    procedure SVOpening(Sender: TObject);
    procedure catMenuItemsCategoryCollapase(Sender: TObject; const Category: TButtonCategory);
    procedure imgMenuClick(Sender: TObject);
    procedure actHomeExecute(Sender: TObject);
    procedure actWindExecute(Sender: TObject);
    procedure actDesignExecute(Sender: TObject);
    procedure catMenuItemsSelectedItemChange(Sender: TObject; const Button: TButtonItem);
    procedure actModelExecute(Sender: TObject);
    procedure actAnalyseExecute(Sender: TObject);
    procedure actHelpExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonDesignClick(Sender: TObject);
    procedure VirtualStringTree1Click(Sender: TObject);
    procedure VirtualStringTree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VirtualStringTree1GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: TImageIndex);
    procedure VirtualStringTree1GetNodeDataSize(Sender: TBaseVirtualTree;
      var NodeDataSize: Integer);
    procedure VirtualStringTree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure VirtualStringTree1PaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType);
    procedure VirtualStringTree1Editing(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure VirtualStringTree1HeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
    procedure VirtualStringTree1CompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode;
      Column: TColumnIndex; var Result: Integer);
    procedure VirtualStringTree1Edited(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex);
    procedure VirtualStringTree1InitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
      var InitialStates: TVirtualNodeInitStates);
    procedure VirtualStringTree1GetHint(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: string);
    procedure VirtualStringTree1NewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: string);
    procedure ButtonWindClick(Sender: TObject);
    procedure ButtonModelClick(Sender: TObject);
    procedure ButtonDesignReportClick(Sender: TObject);
    procedure actReportExecute(Sender: TObject);
    procedure SearchBoxByNameParamInvokeSearch(Sender: TObject);
    procedure SearchBoxByNameParamChange(Sender: TObject);
  private
    Item: TListItem;
    Sub: Integer;
    NewSearch: Boolean;
    foundNode: PVirtualNode;
    procedure Log(const Msg: string);
    procedure SearchForText(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer;
      var Abort: Boolean);
  public
    procedure Configuration;
    procedure UpdateVirtualTreeView(CategoryName: String);
  end;

var
  MainForm: TMainForm;

implementation

uses
  Vcl.Themes, PreferencesUnit, GeneratorInfoObjectUnit, Math;

{$R *.dfm}

procedure TMainForm.Configuration;
begin
  { TODO : ����� ����� ��������� �������� ���� VirtualTreeView }
end;

procedure TMainForm.UpdateVirtualTreeView(CategoryName: String);
// ����� ����������� TVirtualTreeView
var
  I: Integer;
  MyParam: PParamRecord;
  SL: TStringList;
  Node: PVirtualNode;
begin
  { Node := VirtualStringTree1.GetFirst();

    while Node <> nil do
    begin
    MyParam := VirtualStringTree1.GetNodeData(Node);
    if (MyParam^.ParamValue <> '')  and (GeneratorInfo.GetParamState(MyParam^.ParamName) <>
    ICON_LOCKED)  then
    GeneratorInfo.SetParamValue(MyParam^.ParamName, MyParam^.ParamValue);

    Node := VirtualStringTree1.GetNext(Node);
    end; }

  VirtualStringTree1.Clear;

  VirtualStringTree1.BeginUpdate;
  try
    SL := GeneratorInfo.GetCategoryParams(CategoryName);

    Taskbar1.ProgressMaxValue := SL.Count - 1;
    Taskbar1.ProgressState := TTaskBarProgressState.Normal;

    for I := 0 to SL.Count - 1 do
    begin
      Taskbar1.ProgressValue := I;

      MyParam := VirtualStringTree1.GetNodeData(VirtualStringTree1.AddChild(nil));
      MyParam^.ParamName := SL.Strings[I];
      MyParam^.ParamState := GeneratorInfo.GetParamState(MyParam^.ParamName);
      MyParam^.ParamDescription := GeneratorInfo.GetParamDesc(MyParam^.ParamName);

      if GeneratorInfo.GetParamState(MyParam^.ParamName) = ICON_UNLOCKED then
        MyParam^.ParamValue := GeneratorInfo.GetParamValue(MyParam^.ParamName);
      if GeneratorInfo.GetParamState(MyParam^.ParamName) = ICON_LOCKED then
        MyParam^.ParamValue := GeneratorInfo.GetParamCalculatedValue(MyParam^.ParamName);
      if (GeneratorInfo.GetParamState(MyParam^.ParamName) = ICON_INPUT) and (USE_DEFAULT_VALUES = 0)
      then
        MyParam^.ParamValue := '';
      if (GeneratorInfo.GetParamState(MyParam^.ParamName) = ICON_INPUT) and (USE_DEFAULT_VALUES = 1)
      then
        MyParam^.ParamValue := GeneratorInfo.GetParamDefaultValue(MyParam^.ParamName);;

      MyParam^.ParamDim := GeneratorInfo.GetParamDim(MyParam^.ParamName);
      MyParam^.ParamHint := GeneratorInfo.GetParamFullHint(MyParam^.ParamName);
    end;

  finally
    FreeAndNil(SL);
    VirtualStringTree1.EndUpdate;
    Taskbar1.ProgressState := TTaskBarProgressState.None;
  end;
end;

// ��� ��������� ������ �� ������ ������
procedure TMainForm.VirtualStringTree1Click(Sender: TObject);
var
  Node: PVirtualNode;
begin
  // �������� ��������� ���������� ����� ������ (���� ��� ����)
  Node := VirtualStringTree1.GetFirstSelected();

  // ��������� � ����� �������������� ������� �� ���������
  // (������� �� ������ ������� ��� ������, ����������� ������� ��������)
  if (Node <> nil) then
    VirtualStringTree1.EditNode(Node, VALUE_COLUMN_INDEX);
end;

procedure TMainForm.VirtualStringTree1CompareNodes(Sender: TBaseVirtualTree;
  Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  Data1, Data2: PParamRecord;
begin
  Data1 := Sender.GetNodeData(Node1);
  Data2 := Sender.GetNodeData(Node2);
  if (not Assigned(Data1)) or (not Assigned(Data2)) then
    Result := 0
  else
    case Column of
      NAME_COLUMN_INDEX:
        Result := CompareText(Data1.ParamName, Data2.ParamName);
      DESC_COLUMN_INDEX:
        Result := CompareText(Data1.ParamDescription, Data2.ParamDescription);
      VALUE_COLUMN_INDEX:
        if (Data1.ParamValue = '') or (Data2.ParamValue = '') then
          Result := 0
        else
          Result := CompareValue(StrToFloatSafe(Data1.ParamValue),
            StrToFloatSafe(Data2.ParamValue));
    end;
end;

// ����������� ��������
procedure TMainForm.VirtualStringTree1Edited(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex);
var
  Data: PParamRecord;
begin
  // �������� ����� ���������� ������
  Sender.BeginUpdate;

  // �������� ����� ��������
  Data := Sender.GetNodeData(Node);

  // ��������� ������ � ������ ������
  GeneratorInfo.SetParamValue(Data^.ParamName, Data^.ParamValue);

  // ��������� ���������, ������� �� ������ ���������� ������
  Sender.EndUpdate;
end;

// ��������� �������������� � ��������
procedure TMainForm.VirtualStringTree1Editing(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
  // ��������� �������������� ������ � ������� ��������
  Allowed := Column = VALUE_COLUMN_INDEX;
end;

// ������� ������ ����� �������� �����
// ���������, ����� �������� ������ ������
procedure TMainForm.VirtualStringTree1FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  MyParam: PParamRecord;
begin
  MyParam := Sender.GetNodeData(Node);
  if Assigned(MyParam) then
    Finalize(MyParam^);
end;

procedure TMainForm.VirtualStringTree1GetHint(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: string);
var
  MyParam: PParamRecord;
begin
  MyParam := Sender.GetNodeData(Node);
  // Add a dummy hint to the normal hint to demonstrate multiline hints.
  if (Column = DESC_COLUMN_INDEX) and (Node.Parent <> Sender.RootNode) then
  begin
    HintText := MyParam^.ParamDescription;
    { Related to #Issue 623
      Observed when solving issue #623. For hmToolTip, the multi-line mode
      depends on the node's multi-lin emode. Hence, append a line only
      if not hmToolTip. Otherwise, if you must append lines, force the
      lineBreakStyle := hlbForceMultiLine for hmToolTip.
    }
    if (Sender as TVirtualStringTree).Hintmode <> hmTooltip then
      HintText := HintText + #13 + '(Multiline hints are supported too).';
  end;
end;

// ��������� ������ ����� ������
procedure TMainForm.VirtualStringTree1GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: TImageIndex);
var
  MyParam: PParamRecord;
begin

  { TODO : ���� ����� �������� ������ � ����� ��������, ������ �������� �� ����� }

  if (Kind in [ikNormal, ikSelected]) then
  begin
    MyParam := Sender.GetNodeData(Node);
    if Column = STATE_COLUMN_INDEX then
      ImageIndex := MyParam^.ParamState
    else
      ImageIndex := -1;
  end
  else
    ImageIndex := -1;
end;

// ���������� ������ ������, ���������� � ��������� ��������, �������� �� ������������
procedure TMainForm.VirtualStringTree1GetNodeDataSize(Sender: TBaseVirtualTree;
  var NodeDataSize: Integer);
begin
  NodeDataSize := szParamRecord;
end;

// ���������� �������� ������
procedure TMainForm.VirtualStringTree1GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  MyParam: PParamRecord;
begin
  // �������� ��������� �� ������ ������� ������
  MyParam := Sender.GetNodeData(Node);

  // ���� ����������� ������� ���
  case Column of
    // ��� ��������� (�������� PMM) ��������� ������ ���������� �����
    NAME_COLUMN_INDEX:
      if TextType = ttNormal then
        CellText := MyParam^.ParamName;
    // �������� ��������� (�������� "��������") ��������� ������ ���������� �����
    DESC_COLUMN_INDEX:
      if TextType = ttNormal then
        CellText := MyParam^.ParamDescription;
    // �������� ��������� (�������� "300 ��") � ���������� ����� ����� ��������,
    // � � ����������� ����������� (��� ������� ������ �� ������ ����������� ����� ��
    // ������������, �.�. ��� ������ ��������)
    VALUE_COLUMN_INDEX:
      begin
        // ���� �������� ��� - ����������� ���� �� �����
        if MyParam^.ParamValue <> '' then
          case TextType of
            ttNormal:
              CellText := MyParam^.ParamValue;
            ttStatic:
              CellText := MyParam^.ParamDim;
          end
        else
          CellText := '';
      end;
    // �������� ��������� (�������� "����������� �������� ����������")
    // ��������� ������ ���������� �����
    HINT_COLUMN_INDEX:
      if TextType = ttNormal then
        CellText := MyParam^.ParamHint;
  end;
end;

// ��� ������ �� ��������� �������
procedure TMainForm.VirtualStringTree1HeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
begin
  // ��������� � ����� ���������� ������
  Sender.Treeview.BeginUpdate;
  try
    // ��������� ���������� �� ���������� ������� SortDirection - ����������� ����������
    Sender.Treeview.SortTree(HitInfo.Column, Sender.SortDirection, True);
  finally

    // ��������� ���������, ������� �� ������ ���������� ������
    Sender.Treeview.EndUpdate;
  end;
end;

procedure TMainForm.VirtualStringTree1InitNode(Sender: TBaseVirtualTree;
  ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  // �������� ����������������, �� ��������� ����������� ������ ���������
  // Include(InitialStates, ivsMultiline);
end;

// ��� ����� ������������� ������ � ������� "��������"
procedure TMainForm.VirtualStringTree1NewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; NewText: string);
var
  MyParam: PParamRecord;
  NewValue: Extended;
begin
  // �������� ��������� �� ������� ������
  MyParam := Sender.GetNodeData(Node);
  try
    // �������� � ������������ ���������� ��� �����, ��� ��� ������������
    NewValue := StrToFloatSafe(NewText);
  except
    on E: EConvertError do
      raise EConvertError.CreateFmt('��� ����� �������� ��������� %s �� ����� �� �����',
        [MyParam^.ParamName]);
  end;

  try
    // �������� ��������� �� ������ MyParam � ������� ������ � ���������� � ����
    // � ������ ParamName �������� ������������� ��������
    GeneratorInfo.SetParamValue(MyParam^.ParamName, NewValue);
    MyParam^.ParamValue := NewText;
  except
    on E: Exception do
    begin
      // ��������� �������� ���������
      Sender.EndEditNode;
      raise Exception.Create(E.Message);
    end;
  end;

end;

// ������������ ������ ������
procedure TMainForm.VirtualStringTree1PaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType);
var
  MyParam: PParamRecord;
begin
  // �������� ��������� �� ������� ������
  MyParam := Sender.GetNodeData(Node);
  // ���� ������ ���������� � ����������� �� ������ ������� ������ � ���� ����,
  // ��������� ������������ ��� ������
  if Assigned(MyParam) then
    case Column of
      NAME_COLUMN_INDEX:
        // TargetCanvas.Font.Color := clBlue
        ;
      DESC_COLUMN_INDEX:
        // TargetCanvas.Font.Color := clRed
        ;
      VALUE_COLUMN_INDEX:
        begin
          // TargetCanvas.Font.Color := clGrayText;
          TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold];
        end;
      HINT_COLUMN_INDEX:
        begin
          // TargetCanvas.Font.Color := clMaroon;
          TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsItalic];
        end;
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  StyleName: string;
begin
  Caption := APP_NAME;

  lblTitle.Caption := ACT_HOME_CAPTION;

  SV.UseAnimation := SV_USE_ANIMATION;
  SV.AnimationDelay := SV_ANIMATION_DELAY;
  SV.AnimationStep := SV_ANIMATION_STEP;
  SV.DisplayMode := SV_DISPLAY_MODE;
  SV.Placement := SV_PLACEMENT;
  SV.CloseStyle := SV_CLOSE_STYLE;

  actHome.Caption := ACT_HOME_CAPTION;
  actHome.Hint := ACT_HOME_HINT;
  actWind.Caption := ACT_WIND_CAPTION;
  actWind.Hint := ACT_WIND_HINT;
  actDesign.Caption := ACT_DESIGN_CAPTION;
  actDesign.Hint := ACT_DESIGN_HINT;
  actOpt.Caption := ACT_OPT_CAPTION;
  actOpt.Hint := ACT_OPT_HINT;
  actModel.Caption := ACT_MODEL_CAPTION;
  actModel.Hint := ACT_MODEL_HINT;
  actAnalyse.Caption := ACT_ANALYSE_CAPTION;
  actAnalyse.Hint := ACT_ANALYSE_HINT;
  actHelp.Caption := ACT_HELP_CAPTION;
  actHelp.Hint := ACT_HELP_HINT;

  GeneratorInfo := TGeneratorInfo.Create;
  GeneratorInfo.Xml.SaveToFile('GeneratorTemplateChanged.xml');

  VirtualStringTree1.NodeDataSize := szParamRecord;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  Configuration;
  actWindExecute(Sender);
end;

procedure TMainForm.imgMenuClick(Sender: TObject);
begin
  if SV.Opened then
    SV.Close
  else
    SV.Open;
end;

procedure TMainForm.SearchBoxByNameParamChange(Sender: TObject);
// ��� ��������� ������ � ������ ������
begin
  // �������� ����� �����, �.�. ����� ����� ����� � ������ ������
  NewSearch := True;
end;

procedure TMainForm.SearchBoxByNameParamInvokeSearch(Sender: TObject);
// ��� ������ ������ (������������ ����� Enter ��� ������� �� ����)
var
  tempNode: PVirtualNode;
  MyParam: PParamRecord;
begin
  inherited;
  // ���� ����� ����� (��� ������� ����� � ������ ������), �� ���� � ������ ������ ������
  if NewSearch then
  begin
    tempNode := VirtualStringTree1.GetFirst();
  end
  else
  // ����� ���� ������� � ���������� ����� ���� foundNode (������ �� ���������� ����� ����)
  begin
    tempNode := VirtualStringTree1.GetNext(foundNode);
  end;

  // ��������� �� ����� ������ ��������� ���� �� �������
  while Assigned(tempNode) do
  begin
    MyParam := VirtualStringTree1.GetNodeData(tempNode);

    // ���� ��� ������ ���� � ������� �������, ������ �� ���� �����, ��������, ����������
    { TODO : ����� ����� ��� � �������������� � ����� ���� }
    if AnsiPos(AnsiLowerCase(string(SearchBoxByNameParam.Text)),
      AnsiLowerCase(MyParam^.ParamDescription)) <> 0 then
    begin
      VirtualStringTree1.FocusedNode := tempNode;
      VirtualStringTree1.Selected[tempNode] := True;
      foundNode := tempNode;
      NewSearch := False;
      Break;
    end;
    // ��� � ���������� ����
    tempNode := VirtualStringTree1.GetNext(tempNode);
  end;
end;

procedure TMainForm.SearchForText(Sender: TBaseVirtualTree; Node: PVirtualNode; Data: Pointer;
  var Abort: Boolean);
var
  NodeData: PParamRecord; // replace by your record structure
begin
  NodeData := Sender.GetNodeData(Node);
  Abort := AnsiPos(AnsiLowerCase(string(Data)), AnsiLowerCase(NodeData.ParamDescription)) <> 0;
  // ������� Pos ���������� ����� ������� � �������� ���������� ��������� ��������� string(Data)
  // � ������ NodeData.ParamDescription, ���� ��������� ���� - ������ �� ������� � Abort = false
  // ���� Abort = True ����� ������������
end;

procedure TMainForm.SVClosed(Sender: TObject);
begin
  // When TSplitView is closed, adjust ButtonOptions and Width
  catMenuItems.ButtonOptions := catMenuItems.ButtonOptions - [boShowCaptions];
  if SV.CloseStyle = svcCompact then
    catMenuItems.Width := SV.CompactWidth;
end;

procedure TMainForm.SVClosing(Sender: TObject);
begin
  //
end;

procedure TMainForm.SVOpened(Sender: TObject);
begin
  // When not animating, change size of catMenuItems when TSplitView is opened
  catMenuItems.ButtonOptions := catMenuItems.ButtonOptions + [boShowCaptions];
  catMenuItems.Width := SV.OpenedWidth;
end;

procedure TMainForm.SVOpening(Sender: TObject);
begin
  // When animating, change size of catMenuItems at the beginning of open
  catMenuItems.ButtonOptions := catMenuItems.ButtonOptions + [boShowCaptions];
  catMenuItems.Width := SV.OpenedWidth;
end;

procedure TMainForm.actHelpExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actHelp.Caption);
  MainPageControl.ActivePageIndex := ACT_HELP_INDEX;
end;

procedure TMainForm.actHomeExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actHome.Caption);
  MainPageControl.ActivePageIndex := ACT_HOME_INDEX;
end;

procedure TMainForm.actModelExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actModel.Caption);
  MainPageControl.ActivePageIndex := ACT_MODEL_INDEX;
end;

procedure TMainForm.actReportExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actReport.Caption);
  GeneratorInfo.SaveWordReport('Generator.docx')
end;

procedure TMainForm.actWindExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actWind.Caption);
  MainPageControl.ActivePageIndex := ACT_WIND_INDEX;
  UpdateVirtualTreeView('WindInput');
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
  GeneratorInfo.SaveFullReport(ExtractFilePath(Application.ExeName) + 'Generator.xml');
end;

procedure TMainForm.ButtonDesignClick(Sender: TObject);
begin
  GeneratorInfo.DesignExecute;
  actModelExecute(Sender);
  UpdateVirtualTreeView('DesignOutput');
  // MainPageControl.ActivePageIndex := ACT_MODEL_INDEX;
  // GeneratorInfo.SaveWordReport('Generator.docx');
end;

procedure TMainForm.ButtonDesignReportClick(Sender: TObject);
begin
  // ��������� ������������ ��������� ���������� � ���� Word
  GeneratorInfo.SaveWordReport('Generator.docx');
end;

procedure TMainForm.ButtonModelClick(Sender: TObject);
begin
  // GeneratorInfo.ModelExecute;
  actAnalyseExecute(Sender);
end;

procedure TMainForm.ButtonWindClick(Sender: TObject);
begin
  GeneratorInfo.WindExecute;
  actDesignExecute(Sender);
  // UpdateVirtualTreeView('WindOutput');
  // MainPageControl.ActivePageIndex := ACT_DESIGN_INDEX;
  // GeneratorInfo.SaveWordReport('Generator.docx');
end;

procedure TMainForm.actAnalyseExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actAnalyse.Caption);
  MainPageControl.ActivePageIndex := ACT_ANALYSE_INDEX;
end;

procedure TMainForm.actDesignExecute(Sender: TObject);
begin
  Log(DateTimeToStr(Now) + ' ���� ������� ���������: ' + actDesign.Caption);
  MainPageControl.ActivePageIndex := ACT_DESIGN_INDEX;
  UpdateVirtualTreeView('DesignInput');
end;

procedure TMainForm.catMenuItemsCategoryCollapase(Sender: TObject; const Category: TButtonCategory);
begin
  // Prevent the catMenuItems Category group from being collapsed
  catMenuItems.Categories[0].Collapsed := False;
end;

procedure TMainForm.catMenuItemsSelectedItemChange(Sender: TObject; const Button: TButtonItem);
begin
  lblTitle.Caption := Button.Caption;
  // MainPageControl.ActivePageIndex := catMenuItems.SelectedItem.Index;
  if SV.Opened and SV_CLOSE_ON_MENU_CLICK then
    SV.Close;
end;

procedure TMainForm.Log(const Msg: string);
// ����� ���� � ������ �������������� ������
var
  Idx: Integer;
begin
  Idx := lstLog.Items.Add(Msg);
  lstLog.TopIndex := Idx;
end;

end.
