unit DesignCalcUnit;

interface

uses
  System.SysUtils;

type
  TDesignInputData = record
    AL: Extended;
    B0: Extended;
    BL: Extended;
    D11: Extended;
    Di: Extended;
    F1: Extended;
    P: Integer;
    PMM: Extended;
    U: Extended;
  end;

  TDesignOutputData = record
    AL: Extended;
    AL2: Extended;
    ALW: Extended;
    B0: Extended;
    B02: Extended;
    B5: Extended;
    bbp: Extended;
    BL: Extended;
    bmm: Extended;
    bz1: Extended;
    cpr: Extended;
    cs: Extended;
    CS1: Extended;
    D11: Extended;
    Dcp: Extended;
    De: Extended;
    Di: Extended;
    DN: Extended;
    Dnp: Extended;
    e0: Extended;
    edk: Extended;
    F1: Extended;
    f11: Extended;
    FAD: Extended;
    Fbo: Extended;
    fdk: Extended;
    FFM: Extended;
    Fj: Extended;
    FL: Extended;
    fmk: Extended;
    fmk1: Extended;
    Fn: Extended;
    Fnb: Extended;
    fq: Extended;
    fsm: Extended;
    Fsnb: Extended;
    Fz1: Extended;
    H0: Extended;
    h1: Extended;
    H2: Extended;
    H3: Extended;
    H45: Extended;
    H5: Extended;
    H8: Extended;
    H9: Extended;
    ha: Extended;
    HGEN: Extended;
    hm: Extended;
    HU1: Extended;
    lm: Extended;
    lsl: Extended;
    P: Integer;
    PCZ: Extended;
    Pe: Extended;
    PL: Extended;
    PL2: Extended;
    PMax: Extended;
    PMM: Extended;
    PX1: Extended;
    RM: Extended;
    SUMP: Extended;
    t1: Extended;
    tay: Extended;
    TOKA: Extended;
    TOKR: Extended;
    U: Extended;
    U11: Extended;
    UserName: String;
    vm: Extended;
    wr: Extended;
    XAD: Extended;
    xaq: Extended;
    xd: Extended;
    xq: Extended;
    xs: Extended;
    xsp: Extended;
  end;

  TParam = record
    Delta, B_Delta, D_i, L_Delta, D_11, G_Mag, G_Gen, P_Max, U_11, D_Nar, X_S: Extended;
  end;

function MainCalc(Input: TDesignInputData; var Output: TDesignOutputData): HRESULT;
function OptCalc(Input: TDesignInputData; var Output: TParam): HRESULT;
function LPiTau(Step: Extended): Extended;

implementation

uses ComplexUnit;

resourcestring
  StrUorCSNFisNILL = '��������� ���������� � ����������� �������� �� ����� �' + '��� ����� ����';
  StrD11isNILL = '�������� ������� �� ����� ���� ����� ����';
  StrFindDNPisWRONG = '� ���������� ���������� ��������� FindDNP ������� ���' + '���� ���� �������������';
  StrFindPL1isWRONG = '� ���������� FindPL1 ������� ��������� ����� ��������' + '������ �����';
  StrFindBZisWRONG = '� ���������� ���������� FindBz ������ ����� ������� ��' + '��� ������ ����';
  StrT1isNILL = '�������� ��� ������� �� ����� ���� ����� ����';
  StrPandMisNILL = '�������� � ����� ��� �� ����� ���� ����� ����';
  StrFindKZisWRONG = '� ���������� ���������� FindKz ��������� �������� ����' + '�� ����';

const
  BB1: array [0 .. 39] of Extended = (0.0552, 0.0616, 0.0707, 0.0779, 0.0881, 0.099, 0.1104, 0.1257, 0.1419, 0.159,
    0.1772, 0.1963, 0.221, 0.246, 0.283, 0.312, 0.353, 0.396, 0.442, 0.503, 0.567, 0.636, 0.709, 0.785, 0.883, 0.985,
    1.0945, 1.227, 1.368, 1.539, 1.767, 2.011, 2.27, 2.54, 2.83, 3.14, 3.53, 3.94, 4.36, 4.91);
  BB2: array [0 .. 39] of Extended = (0.3, 0.315, 0.335, 0.35, 0.37, 0.395, 0.415, 0.44, 0.465, 0.49, 0.515, 0.545,
    0.585, 0.615, 0.655, 0.69, 0.73, 0.77, 0.815, 0.869, 0.915, 0.965, 1.015, 1.08, 1.14, 1.2, 1.26, 1.33, 1.405, 1.485,
    1.585, 1.685, 1.785, 1.895, 1.995, 2.095, 2.22, 2.34, 2.46, 2.6);
  BB3: array [0 .. 4] of Extended = (0.0005, 0.001, 0.0015, 0.002, 0.0025);
  BB4: array [0 .. 97] of Extended = (40.5, 43.2, 45.9, 48.6, 51.3, 54.0, 56.7, 59.4, 62.1, 64.8, 67.5, 70.2, 73.0,
    75.6, 78.3, 81.0, 85.0, 89.0, 93.0, 97.0, 110.0, 114.0, 118.0, 122.0, 126.0, 130.0, 134.0, 138.0, 142.0, 146.0,
    152.0, 158.0, 164.0, 170.0, 176.0, 182.0, 188.0, 195.0, 201.0, 207.0, 213.0, 219.0, 225.0, 231.0, 237.0, 243.0,
    249.0, 255.0, 261.0, 267.0, 275.0, 283.0, 291.0, 300.0, 310.0, 320.0, 332.0, 344.0, 358.0, 374.0, 390.0, 414.0,
    438.0, 464.0, 492.0, 520.0, 566.0, 610.0, 665.0, 732.0, 800.0, 890.0, 990.0, 1.015, 1.045, 1.08, 1.112, 1.145,
    1.175, 1.22, 1.26, 1.3, 1.35, 1.393, 1.45, 1.49, 1.53, 1.595, 1.645, 1.7, 1.75, 1.835, 1.92, 2.01, 2.11, 2.27,
    2.45, 2.56);
  BB5: array [0 .. 27] of Extended = (0.25, 0.3, 0.35, 0.4125, 0.475, 0.5375, 0.575, 0.6375, 0.7, 0.7625, 0.825, 0.905,
    1.005, 1.105, 1.225, 1.35, 1.475, 1.6, 1.725, 1.85, 1.952, 2.1, 2.225, 2.350, 2.475, 2.6, 2.725, 2.85);
  BB6: array [0 .. 16] of Extended = (81, 91, 110, 120, 130, 140, 152, 167, 182, 198, 243, 258, 275, 295, 320,
    350, 390);
  BB7: array [0 .. 97] of Extended = (400, 404, 417, 426, 434, 443, 452, 461, 470, 479, 488, 497, 506, 516, 525, 535,
    544, 554, 564, 574, 584, 593, 603, 613, 623, 632, 642, 652, 662, 672, 682, 693, 703, 724, 734, 745, 755, 766, 776,
    787, 798, 810, 823, 835, 848, 850, 873, 885, 898, 911, 924, 938, 953, 969, 986, 1004, 1022, 1039, 1056, 1073, 1090,
    1108, 1127, 1147, 1167, 1187, 1207, 1227, 1248, 1269, 1290, 1315, 1340, 1370, 1400, 1430, 1460, 1490, 1520, 1555,
    1590, 1630, 1670, 1720, 1760, 1810, 1860, 1920, 1970, 2030, 2090, 2160, 2230, 2300, 2370, 2440, 2530, 2620);
  BB8: array [0 .. 4] of Extended = (0.0005, 0.0001, 0.0015, 0.002, 0.0025);
  BB9: array [0 .. 40] of Extended = (500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000,
    13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 21000, 22000, 23000, 24000, 25000, 26000, 27000, 28000,
    29000, 30000, 31000, 32000, 33000, 34000, 35000, 36000, 37000, 38000, 39000, 40000);
  BB10: array [0 .. 40] of Extended = (2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43);

function LPiTau(Step: Extended): Extended;
begin
  Result := Step * (1 + random(10) / 10);
end;

function FindHZ(bz1: Extended): Extended;
var
  i: Integer;
  FM: Extended;
begin
  Result := -1;
  for i := 0 to Length(BB4) - 2 do
  begin
    FM := 0.3 + 0.02 * (i + 1) - 0.01;
    if bz1 < FM then
    begin
      Result := BB4[i];
      Exit;
    end;
    if bz1 <= FM + 0.01 then
    begin
      Result := BB4[i + 1];
      Exit;
    end;
  end;
end;

function FindHj1(Bj1: Extended): Extended;
var
  i: Integer;
  FM: Extended;
begin
  Result := -1;
  for i := 0 to Length(BB4) - 1 do
  begin
    FM := 0.3 + 0.02 * (i + 1) - 0.01;
    if Bj1 < FM then
    begin
      Result := BB4[i];
      Exit;
    end;
    if Bj1 <= FM + 0.01 then
    begin
      Result := BB4[i + 1];
      Exit;
    end;
  end;
end;

function FindHp(BL: Extended): Extended;
var
  i: Integer;
  FM: Extended;
begin
  Result := -1;
  for i := 0 to Length(BB7) - 1 do
  begin
    FM := 0.5 + 0.01 * (i + 1) - 0.01;
    if BL < FM then
    begin
      Result := BB7[i];
      Exit;
    end;
    if BL <= FM + 0.01 then
    begin
      Result := BB7[i + 1];
      Exit;
    end;
  end;
end;

function FindPX(bz1: Extended): Extended;
var
  i: Integer;
  FM: Extended;
begin
  Result := -1;
  for i := 0 to Length(BB5) - 1 do
  begin
    FM := 0.47 + 0.05 * (i + 1) - 0.01;
    if bz1 < FM then
    begin
      Result := BB5[i];
      Exit;
    end;

    if bz1 <= FM + 0.01 then
    begin
      Result := BB5[i + 1];
      Exit;
    end;
  end;
end;

function FindHnb(Bnb: Extended): Extended;
var
  i: Integer;
  FM: Extended;
begin
  Result := -1;
  for i := 0 to Length(BB6) - 1 do
  begin
    FM := 0.6 + 0.05 * (i + 1) - 0.01;
    if Bnb < FM then
    begin
      Result := BB6[i];
      Exit;
    end;

    if Bnb <= FM + 0.01 then
    begin
      Result := BB6[i + 1];
      Exit;
    end;
  end;
end;

function FindDNP(PL: Extended): Extended;
var
  i: Integer;
  z: Extended;
begin
  Result := -1;
  i := 0;
  z := 0;
  while PL > z do
  begin
    z := BB1[i];
    Result := BB2[i];
    i := i + 1;
  end;
end;

function FindPL1(PL: Extended): Extended;
var
  i: Integer;
  z: Extended;
begin
  Result := -1;
  i := 0;
  z := 0;
  while PL > z do
  begin
    z := BB1[i];
    Result := BB1[i];
    i := i + 1;
  end;
end;

function FindBz(bz: Extended): Extended;
var
  i: Integer;
  z: Extended;
begin
  Result := -1;
  i := 0;
  z := 0;
  while bz > z do
  begin
    z := BB8[i];
    Result := BB8[i];
    i := i + 1;
  end;
end;

function FindKz(PMM: Extended): Extended;
var
  i: Integer;
  z: Extended;
begin
  Result := -1;
  i := 0;
  z := 0;
  while PMM > z do
  begin
    z := BB9[i];
    Result := BB10[i];
    i := i + 1;
  end;
end;

function MainCalc(Input: TDesignInputData; var Output: TDesignOutputData): HRESULT;
var
  i: Integer;
  di2: Extended;
  hz11, Hc, bh, dt, dtt, hnb1, bsnn, Br, De, Dcp, h1, hbm, hk, ha, cpr, Dnp, wr, cs, w, an, fq, alx, ql, t1, B5, dx, hr,
    hr1, hr2, ss2, ss1, tokn1, hu, csnf, as1, Bj1, ha1, PL, B, ds2, b3, tn, cp, cn, tay, tb1, t2, t2e, t1cp, SUMF, hj,
    hz1, KS, HZ2, bz1, BZ2, s, ye, BJ, GAMA, GAM, TOKN, TTK1, OTOK, DDT, ALA, AKL, bb, xmr, H9, H0, H3, H5, H6, H8, H45,
    DN, DM, px, pca, PX1, PCZ, pct, t0, w0, t11, rst, rk, rmr, akr1, TOKR, TOKA, tt, csi, Fi1, bn, Kmu, U11, Tj, per,
    bp1, x, y, Anu, AA, AB, S11, AP2, tz1, nu, HKR, N, bz, CS1, e0, Fn, ALAP, lsl, ALAL, FL, FFS, FFM, lm, HMK, bmm,
    BMO, hm, vm, KAR, XAD, xs, xq, xd, ALW, RM, HGEN, H2, xaq, Pe, SUMP, FI, FII, PMax, HU1, KAD, Fz1, Hj1, Fj, Fbo,
    f11, ASMP, ASNP, fsm, FMO, SM, xsp, IDK, edk, fdk, FAD, FADK, F1K, FSK, fmk, fmk1, FFMK, Fc, Fotn, XX, m0, XXO, bbp,
    HHO, BB0, BMK, PL2, FP, Di, B11, BL: Extended;

  z2, m: Integer;

  TOKJ, TOKD, TOKQ, UJ: TComplex;
begin

  //
  // �������� ������
  //

  Bj1 := 0.9; // �������� � ���� �������.
  b3 := 0.0015; // ������ ����� �������  cm kopy1
  HKR := 0.005; // ������ ���������� ������.
  csnf := 0.8; // ����������� ��������.
  Fi1 := 0.643501; // ���� ��.
  N := 2; // ����� ����������� � ���� �������..
  m := 3; // ����� ���.
  dtt := 0.0002; // ������� ������� ��������.
  dt := 0.0001; // ��������������� �����
  alx := 0.7; // ����������� ���������� ������.
  Br := 1.15; // ������ ��������� �����������.
  KAD := 0.85; // ����������� ������� ����� �� ���������� ���.
  Dnp := 0.00062; // ������� �������.
  Hc := 1000000; // ������������� ���� �������.
  m0 := 4 * PI * 0.0000001; // ��/� - ��������� ����������.

  //
  // 1.������ ��������� ����
  //

  if (Input.U = 0) or (csnf = 0) then
    raise Exception.Create(StrUorCSNFisNILL);

  tokn1 := Input.PMM / (Input.U * csnf * 3);
  // ����������� ��� ������� ������� � ���������������� ����������� , �

  if Input.D11 = 0 then
    raise Exception.Create(StrD11isNILL);

  PL := tokn1 / Input.D11; // ��������� ������� ������� ������� �������.

  Dnp := FindDNP(PL) / 1000; // ������� �������.

  if Dnp <= 0 then
    raise Exception.Create(StrFindDNPisWRONG);

  PL2 := FindPL1(PL); // ������� ��������������� ��������.

  if PL2 <= 0 then
    raise Exception.Create(StrFindPL1isWRONG);

  B5 := N * Dnp + dtt; // ������ ���� �������.

  bz := B5 * 0.7;

  bz := FindBz(bz); // ������ ����� �������, �

  if bz <= 0 then
    raise Exception.Create(StrFindBZisWRONG);

  t1 := bz + B5; // �������� ��� ������� �� ����������� ��������

  if t1 = 0 then
    raise Exception.Create(StrT1isNILL);

  CS1 := (PI * Input.Di) / t1; // ����� ������ �������

  if (Input.P = 0) or (m = 0) then
    raise Exception.Create(StrPandMisNILL);

  cs := trunc(CS1 / (2 * Input.P * m)) * 2 * Input.P * m;

  if cs <= 2 * Input.P * m then
    cs := cs * 2;

  CS1 := cs + FindKz(Input.PMM); // ���������� ����� ������ �������

  if FindKz(Input.PMM) <= 0 then
    raise Exception.Create(StrFindKZisWRONG);

  Di := CS1 * t1 / PI; // ���������� �������� ������������ ��������� ��������.

  // DI:=round(DI*1000)/1000;                // ����������!!!

  Dcp := Input.AL + Di; // ������� �������� �������
  De := Input.AL + Dcp; // �������� �������� �������
  DN := De + 2 * (cs * Dnp / (2 * Input.P * m) + 0.002);
  // �������� ������� ����������

  // DN:=round(DN*1000)/1000;                // ����������!!!

  as1 := Input.AL + 0.002; // ����� ���� �������,�
  tay := (PI * Dcp) / (2 * Input.P); // �������� ������� �� �������� ��������,�
  ql := (PI * (De * De - Di * Di)) / (8 * Input.P); // ������� �������� ������.
  // ������ ��������� ����������� , �
  fq := Input.BL * alx * tay * Input.AL;
  // ��������� �����.ALX-����������� ��������� ����������.
  w := (1.15 * 0.5 * Input.U) / (4.44 * Input.F1 * 0.95 * fq);
  // ����� ������ ���� �������
  wr := w;
  cpr := 2 * round(3 * wr / cs); // ����� ����������� � ���� �������
  cp := cpr;
  wr := cpr * cs / 6; // ���������� ����� ������ ������� �������.

  BL := (1.15 * Input.U * 0.5) / (4.44 * Input.F1 * 0.95 * alx * tay * Input.AL * wr); // �������� � ������� ������, ��.

  h1 := cpr * Dnp + 0.003 + HKR; // ������ ����� �������.

  bz1 := (BL * ql) / (ql - (B5 * PI * Input.AL * Dcp) / (t1 * 2 * Input.P));
  // �������� � ������ �������.
  e0 := 4.44 * Input.F1 * 0.95 * wr * fq; // ��� ��������� ����.
  Fn := 1.08 * fq; // ��������� ����� ������.

  ha := Fn / (1.9 * as1 * Bj1); //

  ha := ha * 1000; //
  ha1 := ha - trunc(ha); //
  if (ha < 0.25) then
    ha := trunc(ha); // ���������� ������ ���� ������
  if (ha > 0.25) then
    ha := 0.5 + trunc(ha); // ���������� �� ��� ����������
  if (ha1 >= 0.75) then
    ha := 1 + trunc(ha); //
  ha := ha / 1000; //

  if Fn / (1.9 * as1 * Bj1) < 0.0005 then
    ha := 0.0005;

  B11 := Input.B0 * ((t1 + 10 * Input.B0) / (t1 - B5 + 10 * Input.B0)) + dt * ((t1 + 10 * dt) / (t1 - B5 + 10 * dt));
  // ������ �������������� ������.
  ALAP := 0.4 * PI * ((cpr * Dnp) / (3 * B5)) + ((0.5 * h1) / (3 * B5));
  // ����������� �������� �����������.
  lsl := tay + 0.004; // ����� ������� ����� �������.
  ALAL := 0.42 * cs * (lsl - 0.64 * tay) / (6 * Input.AL * Input.P);
  // ���������� �������� �����������.

  // ok


  //
  // 2. ������ ���.
  //

  FAD := 3 * 0.9 * 0.83 * wr * 0.95 * tokn1 / Input.P;
  // ��� ����� �� ���������� ��� ����������.
  FL := 1.6 * B11 * BL * 1E6; // ��� ���������� ������.
  Fz1 := 2 * FindHZ(bz1) * h1; // ��� ������ �������.
  FP := 2 * FindHp(BL) * tay; // ��� ������ �������.

  Fj := 2 * FindHj1(Bj1) * tay; // ��� ���� �������.
  Fbo := FL + Fz1 + Fj + FP; // �������� ���.

  f11 := FL + Fz1 + FP; // ��� ��������.

  // ������ ��������
  lm := Input.AL; // ����� �������, �
  FFM := f11; // ��� �������, �
  HMK := 0.6 * Hc; // ������������� ���������� ����.
  bmm := FFM / HMK; // ������ �������, � ���� ��������� �� ������ �����
  bmm := round(bmm * 1000) / 1000; //
  // ����������
  if bmm < 0.0015 then
    bmm := 0.0015; //

  bbp := tay - bmm; // ������ ������ �� �������� ��������:

  BMO := 1.1;
  hm := BL * tay / BMO; // ������ �������.
  hm := round(hm * 100) / 100; // ����������

  vm := lm * bmm * hm * 2 * Input.P; // ����� �������.
  ASMP := ((5 * lm * hm / (tay - bbp)) + 1.6 * hm) * 0.0000004;
  // ����������� ����������� �������.
  fsm := f11 * ASMP; // ����� ��������� �������, ��
  fmk := fq + fsm; // ��������� ����� ������� , ��
  fmk := round(fmk * 1000) / 1000; // ����������!!!

  // ok

  // 3. ������ �������������//

  t0 := 4 * PI * 1E-7;
  KAR := (t1 + 10 * Input.B0) / (t1 - B5 + 10 * Input.B0);
  B := Input.B0 * KAR + dt * (t1 + 10 * dt) / (t1 - B5 + 10 * dt);
  XAD := (4 * 3 * Input.F1 * t0 * Input.AL * tay * (wr * 0.95) * (wr * 0.95) * KAD) / (PI * B * Input.P);
  // ������� ����������� ������������� ����� �� ���������� ��� ������
  xaq := (XAD / 0.85) * 0.44;
  // ������� ����������� ������������� ����� �� ���������� ���.
  xs := 4 * 2 * 3 * PI * Input.F1 * Input.AL * wr * wr * (ALAL + ALAP) / (cs * 1000000);
  // ����������� ������������� ������� �������.

  xs := round(xs * 1000) / 1000; // ����������!!!

  xq := xaq + xs;
  // ���������� ����������� ������������� ����� �� ���������� ���.
  xd := XAD + xs;
  // ���������� ����������� ������������� ����� �� ���������� ���.
  ALW := 2 * Input.AL + 2 * tay + 0.006; // ������� ����� �����, �
  RM := ALW * wr / (46 * PL2); // �������� ������������� ������� �������,
  xsp := (xs + RM * RM) / xq; // ��������� ������������� ����������� ���� ��.
  IDK := 2 * tokn1; // ���������� ��� �����.
  edk := IDK * xsp; // ��� �������������� �� ���������� ���.
  fdk := fq * edk / e0; // ����� �������������� �� ���������� ���.
  FADK := FAD * IDK / (0.83 * tokn1); // ��� ���������� ������� ����� ��� ��.
  F1K := f11 * edk / e0; // ��� ���������� ������� ������� ��� ��, �
  ASNP := ASMP; // ����������� ����������� �����.
  FSK := (F1K + FADK) * ASNP; // ��������� ����� ������� ������� , �� :
  fmk1 := FSK + fdk; // ��������� ����� ������ , �� :
  FFMK := F1K + FADK; // ��� ������� ��� �� �� ���� �������, �:


  // ok
  // 4. ������ ���� �������� ����������//

  HGEN := hm * 2 + h1 + 2 * Input.B0 + HKR; // ������ ����������, �
  HGEN := round(HGEN * 1000) / 1000; // ����������!!!

  H9 := 6 * 8.90E-3 * ALW * PL2 * wr; // ����� ���������� ���� �������.
  H9 := round(H9 * 1000) / 1000; // ����������!!!

  H5 := 7.8 * 2 * h1 * Input.AL * bz * PI * Dcp * 1E3 / t1;
  // ����� ������������������ ����� ������ �������.
  H5 := round(H5 * 10000) / 10000; // ����������!!!

  H0 := 2 * 7.5 * vm * 1000; // ����� �������� � ������.
  H0 := round(H0 * 10000) / 10000; // ����������!!!

  H2 := PI * (De * De - Di * Di) * 2 * 7.8 * ha * 1000 / 4;
  // ����� ������������������ ����� ���� �������.
  H2 := round(H2 * 10000) / 10000; // ����������!!!

  H8 := (PI * (De * De - Di * Di) * 2 * 7.8 * HKR * 1000 / 4) - (7.8 * HKR * bz * Input.AL * cs * 1000);
  // ����� ������.
  H8 := round(H8 * 1000) / 1000; // ����������!!!

  H3 := PI * (De * De - Di * Di) * 2 * 7.8 * ha * 1000 / 4;
  // ����� ������������������ ����� ���� ������.
  H3 := round(H3 * 1000) / 1000; // ����������!!!

  H45 := H9 + H5 + H0 + H2 + H3 + H8; // ��������� ����� ����������.

  // H45:=H9+H5+H0+H3+H8;                     // ��������� ����� ����������.

  H45 := round(H45 * 1000) / 1000; // ����������!!!

  // ok
  // 4. ������ ������.

  Pe := 3 * RM * tokn1 * tokn1; // ������������� ������ ������� �������.
  PX1 := FindPX(bz1); // ��������� ������ � ������������������ �����
  PCZ := 1.6 * PX1 * (bz1 * bz1) * H5; // ������ �������
  SUMP := 2 * Pe + PCZ + 10; // ��������� ������ ���������


  // 5. ������ ������� ��������������.//

  FI := 55;
  FII := FI / (180 / PI);

  PMax := 3 * Input.U * e0 * (xq * sin(FII) + RM * cos(FII)) / (RM * RM + xd * xq) + 3 * Input.U * Input.U *
    (xd * sin(2 * FII) - xq * sin(2 * FII)) / (2 * RM * RM + 2 * xd * xq) - 3 * Input.U * Input.U * RM /
    (RM * RM + xd * xq);

  PMax := round(PMax * 1000) / 1000; // ����������!!!

  // ������ ��� //

  HU1 := Input.PMM / (Input.PMM + SUMP); // ��� ����������.
  // OK!!!

  // 6. ������ ������� ��������������.//

  TOKA := tokn1 * cos(Fi1);

  TOKR := tokn1 * sin(Fi1);

  TOKJ := Complex(TOKA, -TOKR);

  tt := PI * 30 / 180;

  csi := tt + Fi1;

  TOKD := cMul(TOKJ, Complex(sin(csi), 0));

  TOKQ := cMul(TOKJ, Complex(cos(csi), 0));

  Kmu := Fbo / (1.6 * B * BL * 1000000);

  UJ := cSum(Complex(2 * e0 * Kmu, 0), cSub(cSum(cMul(Complex(0, -xd), TOKD), cMul(Complex(0, -xq), TOKQ)),
    cMul(Complex(RM, 0), TOKJ)));

  U11 := sqrt(Re(UJ) * Re(UJ) + Im(UJ) * Im(UJ));

  U11 := round(U11 * 1000) / 1000; // ����������!!!

  //
  // ����� ������
  //

  Output.AL := Input.AL;
  Output.AL2 := Input.AL;
  Output.B0 := Input.B0;
  Output.B02 := Input.B0;
  Output.D11 := Input.D11;
  Output.PMM := Input.PMM;
  Output.U := Input.U;
  Output.F1 := Input.F1;
  Output.P := Input.P;

  Output.ALW := ALW;
  Output.B5 := B5;
  Output.bbp := bbp;
  Output.BL := BL;
  Output.bmm := bmm;
  Output.bz1 := bz1;
  Output.cpr := cpr;
  Output.cs := cs;
  Output.CS1 := CS1;
  Output.Dcp := Dcp;
  Output.De := De;
  Output.Di := Di;
  Output.DN := DN;
  Output.Dnp := Dnp;
  Output.e0 := e0;
  Output.edk := edk;
  Output.f11 := f11;
  Output.FAD := FAD;
  Output.Fbo := Fbo;
  Output.fdk := fdk;
  Output.FFM := FFM;
  Output.Fj := Fj;
  Output.FL := FL;
  Output.fmk := fmk;
  Output.fmk1 := fmk1;
  Output.Fn := Fn;
  Output.fq := fq;
  Output.fsm := fsm;
  Output.Fz1 := Fz1;
  Output.H0 := H0;
  Output.h1 := h1;
  Output.H2 := H2;
  Output.H3 := H3;
  Output.H45 := H45;
  Output.H5 := H5;
  Output.H8 := H8;
  Output.H9 := H9;
  Output.ha := ha;
  Output.HGEN := HGEN;
  Output.hm := hm;
  Output.HU1 := HU1;
  Output.lm := lm;
  Output.lsl := lsl;
  Output.PCZ := PCZ;
  Output.Pe := Pe;
  Output.PL := PL;
  Output.PL2 := PL2;
  Output.PMax := PMax;
  Output.PX1 := PX1;
  Output.RM := RM;
  Output.SUMP := SUMP;
  Output.t1 := t1;
  Output.tay := tay;
  Output.TOKA := TOKA;
  Output.TOKR := TOKR;
  Output.U11 := U11;
  Output.vm := vm;
  Output.wr := wr;
  Output.XAD := XAD;
  Output.xaq := xaq;
  Output.xd := xd;
  Output.xq := xq;
  Output.xs := xs;
  Output.xsp := xsp;

  // Output.Fnb := Fnb;
  // Output.Fsnb := Fsnb;
  // Output.fmo := fmo;
end;

function OptCalc(Input: TDesignInputData; var Output: TParam): HRESULT;
var
  i: Integer;
  di2: Extended;
  hz11, Hc, bh, dt, dtt, hnb1, bsnn, Br, De, Dcp, h1, hbm, hk, ha, cpr, Dnp, wr, cs, w, an, fq, alx, ql, t1, B5, dx, hr,
    hr1, hr2, ss2, ss1, tokn1, hu, csnf, as1, Bj1, ha1, PL, B, ds2, b3, tn, cp, cn, tay, tb1, t2, t2e, t1cp, SUMF, hj,
    hz1, KS, HZ2, bz1, BZ2, s, ye, BJ, GAMA, GAM, TOKN, TTK1, OTOK, DDT, ALA, AKL, bb, xmr, H9, H0, H3, H5, H6, H8, H45,
    DN, DM, px, pca, PX1, PCZ, pct, t0, w0, t11, rst, rk, rmr, akr1, TOKR, TOKA, tt, csi, Fi1, bn, Kmu, U11, Tj, per,
    bp1, x, y, Anu, AA, AB, S11, AP2, tz1, nu, HKR, N, bz, CS1, e0, Fn, ALAP, lsl, ALAL, FL, FFS, FFM, lm, HMK, bmm,
    BMO, hm, vm, KAR, XAD, xs, xq, xd, ALW, RM, HGEN, H2, xaq, Pe, SUMP, FI, FII, PMax, HU1, KAD, Fz1, Hj1, Fj, Fbo,
    f11, ASMP, ASNP, fsm, FMO, SM, xsp, IDK, edk, fdk, FAD, FADK, F1K, FSK, fmk, fmk1, FFMK, Fc, Fotn, XX, m0, XXO, bbp,
    HHO, BB0, BMK, PL2, FP, Di, B11, BL: Extended;

  z2, m: Integer;

  TOKJ, TOKD, TOKQ, UJ: TComplex;
begin

  //
  // �������� ������
  //

  Bj1 := 0.9; // �������� � ���� �������.
  b3 := 0.0015; // ������ ����� �������  cm kopy1
  HKR := 0.005; // ������ ���������� ������.
  csnf := 0.8; // ����������� ��������.
  Fi1 := 0.643501; // ���� ��.
  N := 2; // ����� ����������� � ���� �������..
  m := 3; // ����� ���.
  dtt := 0.0002; // ������� ������� ��������.
  dt := 0.0001; // ��������������� �����
  alx := 0.7; // ����������� ���������� ������.
  Br := 1.15; // ������ ��������� �����������.
  KAD := 0.85; // ����������� ������� ����� �� ���������� ���.
  Dnp := 0.00062; // ������� �������.
  Hc := 1000000; // ������������� ���� �������.
  m0 := 4 * PI * 0.0000001; // ��/� - ��������� ����������.

  //
  // 1.������ ��������� ����
  //

  if (Input.U = 0) or (csnf = 0) then
    raise Exception.Create(StrUorCSNFisNILL);

  tokn1 := Input.PMM / (Input.U * csnf * 3);
  // ����������� ��� ������� ������� � ���������������� ����������� , �

  if Input.D11 = 0 then
    raise Exception.Create(StrD11isNILL);

  PL := tokn1 / Input.D11; // ��������� ������� ������� ������� �������.

  Dnp := FindDNP(PL) / 1000; // ������� �������.

  if Dnp <= 0 then
    raise Exception.Create(StrFindDNPisWRONG);

  PL2 := FindPL1(PL); // ������� ��������������� ��������.

  if PL2 <= 0 then
    raise Exception.Create(StrFindPL1isWRONG);

  B5 := N * Dnp + dtt; // ������ ���� �������.

  bz := B5 * 0.7;

  bz := FindBz(bz); // ������ ����� �������, �

  if bz <= 0 then
    raise Exception.Create(StrFindBZisWRONG);

  t1 := bz + B5; // �������� ��� ������� �� ����������� ��������

  if t1 = 0 then
    raise Exception.Create(StrT1isNILL);

  CS1 := (PI * Input.Di) / t1; // ����� ������ �������

  if (Input.P = 0) or (m = 0) then
    raise Exception.Create(StrPandMisNILL);

  cs := trunc(CS1 / (2 * Input.P * m)) * 2 * Input.P * m;

  if cs <= 2 * Input.P * m then
    cs := cs * 2;

  CS1 := cs + FindKz(Input.PMM); // ���������� ����� ������ �������

  if FindKz(Input.PMM) <= 0 then
    raise Exception.Create(StrFindKZisWRONG);

  Di := CS1 * t1 / PI; // ���������� �������� ������������ ��������� ��������.

  // DI:=round(DI*1000)/1000;                // ����������!!!

  Dcp := Input.AL + Di; // ������� �������� �������
  De := Input.AL + Dcp; // �������� �������� �������
  DN := De + 2 * (cs * Dnp / (2 * Input.P * m) + 0.002);
  // �������� ������� ����������

  // DN:=round(DN*1000)/1000;                // ����������!!!

  as1 := Input.AL + 0.002; // ����� ���� �������,�
  tay := (PI * Dcp) / (2 * Input.P); // �������� ������� �� �������� ��������,�
  ql := (PI * (De * De - Di * Di)) / (8 * Input.P); // ������� �������� ������.
  // ������ ��������� ����������� , �
  fq := Input.BL * alx * tay * Input.AL;
  // ��������� �����.ALX-����������� ��������� ����������.
  w := (1.15 * 0.5 * Input.U) / (4.44 * Input.F1 * 0.95 * fq);
  // ����� ������ ���� �������
  wr := w;
  cpr := 2 * round(3 * wr / cs); // ����� ����������� � ���� �������
  cp := cpr;
  wr := cpr * cs / 6; // ���������� ����� ������ ������� �������.

  BL := (1.15 * Input.U * 0.5) / (4.44 * Input.F1 * 0.95 * alx * tay * Input.AL * wr); // �������� � ������� ������, ��.

  h1 := cpr * Dnp + 0.003 + HKR; // ������ ����� �������.

  bz1 := (BL * ql) / (ql - (B5 * PI * Input.AL * Dcp) / (t1 * 2 * Input.P));
  // �������� � ������ �������.
  e0 := 4.44 * Input.F1 * 0.95 * wr * fq; // ��� ��������� ����.
  Fn := 1.08 * fq; // ��������� ����� ������.

  ha := Fn / (1.9 * as1 * Bj1); //

  ha := ha * 1000; //
  ha1 := ha - trunc(ha); //
  if (ha < 0.25) then
    ha := trunc(ha); // ���������� ������ ���� ������
  if (ha > 0.25) then
    ha := 0.5 + trunc(ha); // ���������� �� ��� ����������
  if (ha1 >= 0.75) then
    ha := 1 + trunc(ha); //
  ha := ha / 1000; //

  if Fn / (1.9 * as1 * Bj1) < 0.0005 then
    ha := 0.0005;

  B11 := Input.B0 * ((t1 + 10 * Input.B0) / (t1 - B5 + 10 * Input.B0)) + dt * ((t1 + 10 * dt) / (t1 - B5 + 10 * dt));
  // ������ �������������� ������.
  ALAP := 0.4 * PI * ((cpr * Dnp) / (3 * B5)) + ((0.5 * h1) / (3 * B5));
  // ����������� �������� �����������.
  lsl := tay + 0.004; // ����� ������� ����� �������.
  ALAL := 0.42 * cs * (lsl - 0.64 * tay) / (6 * Input.AL * Input.P);
  // ���������� �������� �����������.

  // ok


  //
  // 2. ������ ���.
  //

  FAD := 3 * 0.9 * 0.83 * wr * 0.95 * tokn1 / Input.P;
  // ��� ���� �� ���������� ��� ����������.
  FL := 1.6 * B11 * BL * 1E6; // ��� ���������� ������.
  Fz1 := 2 * FindHZ(bz1) * h1; // ��� ������ �������.
  FP := 2 * FindHp(BL) * tay; // ��� ������ �������.

  Fj := 2 * FindHj1(Bj1) * tay; // ��� ���� �������.
  Fbo := FL + Fz1 + Fj + FP; // �������� ���.

  f11 := FL + Fz1 + FP; // ��� ��������.

  // ������ ��������
  lm := Input.AL; // ����� �������, �
  FFM := f11; // ��� �������, �
  HMK := 0.6 * Hc; // ������������� ���������� ����.
  bmm := FFM / HMK; // ������ �������, � ���� ��������� �� ������ �����
  bmm := round(bmm * 1000) / 1000; //
  // ����������
  if bmm < 0.0015 then
    bmm := 0.0015; //

  bbp := tay - bmm; // ������ ������ �� �������� ��������:

  BMO := 1.1;
  hm := BL * tay / BMO; // ������ �������.
  hm := round(hm * 100) / 100; // ����������

  vm := lm * bmm * hm * 2 * Input.P; // ����� �������.
  ASMP := ((5 * lm * hm / (tay - bbp)) + 1.6 * hm) * 0.0000004;
  // ����������� ����������� �������.
  fsm := f11 * ASMP; // ����� ��������� �������, ��
  fmk := fq + fsm; // ��������� ����� ������� , ��
  fmk := round(fmk * 1000) / 1000; // ����������!!!

  // ok

  // 3. ������ �������������//

  t0 := 4 * PI * 1E-7;
  KAR := (t1 + 10 * Input.B0) / (t1 - B5 + 10 * Input.B0);
  B := Input.B0 * KAR + dt * (t1 + 10 * dt) / (t1 - B5 + 10 * dt);
  XAD := (4 * 3 * Input.F1 * t0 * Input.AL * tay * (wr * 0.95) * (wr * 0.95) * KAD) / (PI * B * Input.P);
  // ������� ����������� ������������� ����� �� ���������� ��� ������
  xaq := (XAD / 0.85) * 0.44;
  // ������� ����������� ������������� ����� �� ���������� ���.
  xs := 4 * 2 * 3 * PI * Input.F1 * Input.AL * wr * wr * (ALAL + ALAP) / (cs * 1000000);
  // ����������� ������������� ������� �������.

  xs := round(xs * 1000) / 1000; // ����������!!!

  xq := xaq + xs;
  // ���������� ����������� ������������� ����� �� ���������� ���.
  xd := XAD + xs;
  // ���������� ����������� ������������� ����� �� ���������� ���.
  ALW := 2 * Input.AL + 2 * tay + 0.006; // ������� ����� �����, �
  RM := ALW * wr / (46 * PL2); // �������� ������������� ������� �������,
  xsp := (xs + RM * RM) / xq; // ��������� ������������� ����������� ���� ��.
  IDK := 2 * tokn1; // ���������� ��� �����.
  edk := IDK * xsp; // ��� �������������� �� ���������� ���.
  fdk := fq * edk / e0; // ����� �������������� �� ���������� ���.
  FADK := FAD * IDK / (0.83 * tokn1); // ��� ���������� ������� ����� ��� ��.
  F1K := f11 * edk / e0; // ��� ���������� ������� ������� ��� ��, �
  ASNP := ASMP; // ����������� ����������� �����.
  FSK := (F1K + FADK) * ASNP; // ��������� ����� ������� ������� , �� :
  fmk1 := FSK + fdk; // ��������� ����� ������ , �� :
  FFMK := F1K + FADK; // ��� ������� ��� �� �� ���� �������, �:


  // ok
  // 4. ������ ���� �������� ����������//

  HGEN := hm * 2 + h1 + 2 * Input.B0 + HKR; // ������ ����������, �
  HGEN := round(HGEN * 1000) / 1000; // ����������!!!

  H9 := 6 * 8.90E-3 * ALW * PL2 * wr; // ����� ���������� ���� �������.
  H9 := round(H9 * 1000) / 1000; // ����������!!!

  H5 := 7.8 * 2 * h1 * Input.AL * bz * PI * Dcp * 1E3 / t1;
  // ����� ������������������ ����� ������ �������.
  H5 := round(H5 * 10000) / 10000; // ����������!!!

  H0 := 2 * 7.5 * vm * 1000; // ����� �������� � ������.
  H0 := round(H0 * 10000) / 10000; // ����������!!!

  H2 := PI * (De * De - Di * Di) * 2 * 7.8 * ha * 1000 / 4;
  // ����� ������������������ ����� ���� �������.
  H2 := round(H2 * 10000) / 10000; // ����������!!!

  H8 := (PI * (De * De - Di * Di) * 2 * 7.8 * HKR * 1000 / 4) - (7.8 * HKR * bz * Input.AL * cs * 1000);
  // ����� ������.
  H8 := round(H8 * 1000) / 1000; // ����������!!!

  H3 := PI * (De * De - Di * Di) * 2 * 7.8 * ha * 1000 / 4;
  // ����� ������������������ ����� ���� ������.
  H3 := round(H3 * 1000) / 1000; // ����������!!!

  H45 := H9 + H5 + H0 + H2 + H3 + H8; // ��������� ����� ����������.

  // H45:=H9+H5+H0+H3+H8;                     // ��������� ����� ����������.

  H45 := round(H45 * 1000) / 1000; // ����������!!!

  // ok
  // 4. ������ ������.

  Pe := 3 * RM * tokn1 * tokn1; // ������������� ������ ������� �������.
  PX1 := FindPX(bz1); // ��������� ������ � ������������������ �����
  PCZ := 1.6 * PX1 * (bz1 * bz1) * H5; // ������ �������
  SUMP := 2 * Pe + PCZ + 10; // ��������� ������ ���������


  // 5. ������ ������� ��������������.//

  FI := 55;
  FII := FI / (180 / PI);

  PMax := 3 * Input.U * e0 * (xq * sin(FII) + RM * cos(FII)) / (RM * RM + xd * xq) + 3 * Input.U * Input.U *
    (xd * sin(2 * FII) - xq * sin(2 * FII)) / (2 * RM * RM + 2 * xd * xq) - 3 * Input.U * Input.U * RM /
    (RM * RM + xd * xq);

  PMax := round(PMax * 1000) / 1000; // ����������!!!

  // ������ ��� //

  HU1 := Input.PMM / (Input.PMM + SUMP); // ��� ����������.
  // OK!!!

  // 6. ������ ������� ��������������.//

  TOKA := tokn1 * cos(Fi1);

  TOKR := tokn1 * sin(Fi1);

  TOKJ := Complex(TOKA, -TOKR);

  tt := PI * 30 / 180;

  csi := tt + Fi1;

  TOKD := cMul(TOKJ, Complex(sin(csi), 0));

  TOKQ := cMul(TOKJ, Complex(cos(csi), 0));

  Kmu := Fbo / (1.6 * B * BL * 1000000);

  UJ := cSum(Complex(2 * e0 * Kmu, 0), cSub(cSum(cMul(Complex(0, -xd), TOKD), cMul(Complex(0, -xq), TOKQ)),
    cMul(Complex(RM, 0), TOKJ)));

  U11 := sqrt(Re(UJ) * Re(UJ) + Im(UJ) * Im(UJ));

  U11 := round(U11 * 1000) / 1000; // ����������!!!

  //
  // ����� ������
  //

  Output.Delta := Input.B0;
  Output.B_Delta := Input.BL;
  Output.D_i := Input.Di;
  Output.L_Delta := Input.AL;
  Output.D_11 := Input.D11;
  Output.G_Mag := H0;
  Output.G_Gen := H45;
  Output.P_Max := PMax;
  Output.U_11 := U11;
  Output.D_Nar := DN;
  Output.X_S := xs;
end;

end.
